<?php
return [
	'paths' => [
		'modules' => [
			'./modules',
		],
		'vendor' => [
			'./'
		],
	],
	'config_cache_dir'    => './config/cache',
	'config_cache_enable' => true,
	'module_manager' => [
		'module_paths' => [
			'./modules'
		],
		'available_modules' => [
			'AdminUser',
			'AdminSystemUser',
			'SystemUser',
			'Common',
			'Market',
			'Gifts',
		],
	],
];