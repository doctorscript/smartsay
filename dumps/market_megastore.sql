-- MySQL dump 10.13  Distrib 5.6.28, for Linux (x86_64)
--
-- Host: localhost    Database: market_megastore
-- ------------------------------------------------------
-- Server version	5.6.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_users`
--

DROP TABLE IF EXISTS `admin_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(32) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_users`
--

LOCK TABLES `admin_users` WRITE;
/*!40000 ALTER TABLE `admin_users` DISABLE KEYS */;
INSERT INTO `admin_users` VALUES (1,'pedro@gmail.com','$2y$10$SYKYp5CN9hMU3Q26U3EZEem4WvIXqGR.8zqX1Cxy/OOA5MGqxmVi.');
/*!40000 ALTER TABLE `admin_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cheques`
--

DROP TABLE IF EXISTS `cheques`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cheques` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bonus` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cheques`
--

LOCK TABLES `cheques` WRITE;
/*!40000 ALTER TABLE `cheques` DISABLE KEYS */;
INSERT INTO `cheques` VALUES (1,0,1,'2016-02-17 14:56:00'),(2,0,1,'2016-02-17 14:57:00'),(3,211,1,'2016-02-17 14:57:00'),(4,278,1,'2016-02-17 15:00:00'),(5,418,1,'2016-02-17 15:01:00'),(6,268,1,'2016-02-17 15:01:00'),(7,0,1,'2016-02-17 15:03:00'),(8,0,2,'2016-02-17 14:56:00'),(9,0,2,'2016-02-17 14:57:00'),(10,211,2,'2016-02-17 14:57:00'),(11,278,2,'2016-02-17 15:00:00'),(12,418,2,'2016-02-17 15:01:00'),(13,268,2,'2016-02-17 15:01:00'),(14,0,2,'2016-02-17 15:03:00'),(15,0,3,'2016-02-17 14:56:00'),(16,0,3,'2016-02-17 14:57:00'),(17,211,3,'2016-02-17 14:57:00'),(18,278,3,'2016-02-17 15:00:00'),(19,418,3,'2016-02-17 15:01:00'),(20,268,3,'2016-02-17 15:01:00'),(21,0,3,'2016-02-17 15:03:00');
/*!40000 ALTER TABLE `cheques` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client_bonus_purchases`
--

DROP TABLE IF EXISTS `client_bonus_purchases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_bonus_purchases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `gift_id` int(11) NOT NULL,
  `purchase_datetime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_bonus_purchases`
--

LOCK TABLES `client_bonus_purchases` WRITE;
/*!40000 ALTER TABLE `client_bonus_purchases` DISABLE KEYS */;
/*!40000 ALTER TABLE `client_bonus_purchases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client_purchases`
--

DROP TABLE IF EXISTS `client_purchases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_purchases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `cheque_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `amount` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_purchases`
--

LOCK TABLES `client_purchases` WRITE;
/*!40000 ALTER TABLE `client_purchases` DISABLE KEYS */;
INSERT INTO `client_purchases` VALUES (1,1,1,4.09,'1'),(2,2,1,10.30,'1'),(3,3,2,23.99,'1'),(4,4,2,1.19,'1'),(5,3,3,19.99,'1'),(6,4,3,1.19,'1'),(7,6,4,1.05,'1'),(8,7,4,6.85,'1'),(9,3,4,19.99,'1'),(10,8,5,14.95,'1'),(11,9,5,26.86,'1'),(12,9,6,26.86,'1'),(13,4,7,1.19,'1'),(14,1,8,4.09,'1'),(15,2,8,10.30,'1'),(16,3,9,23.99,'1'),(17,4,9,1.19,'1'),(18,3,10,19.99,'1'),(19,4,10,1.19,'1'),(20,6,11,1.05,'1'),(21,7,11,6.85,'1'),(22,3,11,19.99,'1'),(23,8,12,14.95,'1'),(24,9,13,26.86,'1'),(25,9,14,26.86,'1'),(26,4,15,1.19,'1'),(27,1,16,4.09,'1'),(28,2,16,10.30,'1'),(29,3,17,23.99,'1'),(30,4,17,1.19,'1'),(31,3,18,19.99,'1'),(32,4,18,1.19,'1'),(33,6,19,1.05,'1'),(34,7,19,6.85,'1'),(35,3,19,19.99,'1'),(36,8,20,14.95,'1'),(37,9,20,26.86,'1'),(38,9,21,26.86,'1'),(39,4,22,1.19,'1');
/*!40000 ALTER TABLE `client_purchases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `surname` varchar(25) NOT NULL,
  `card_number` varchar(50) NOT NULL,
  `phone_number` varchar(14) DEFAULT NULL,
  `card_barcode` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clients`
--

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
INSERT INTO `clients` VALUES (1,'Reshad','Nesibov','1000100000112630\n','+994502650495','1155226'),(2,'Emin','Zamanov','1000100000112631\r\n','+994553533550','1155226'),(3,'Vugar','Aslanov','1000100000112632\r\n','+994554250303','1155226');
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gifts`
--

DROP TABLE IF EXISTS `gifts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gifts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `image` varchar(255) NOT NULL,
  `price` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gifts`
--

LOCK TABLES `gifts` WRITE;
/*!40000 ALTER TABLE `gifts` DISABLE KEYS */;
INSERT INTO `gifts` VALUES (1,'THE GIFT','1.png','100'),(2,'THE GIFT','2.png','100'),(3,'THE GIFT','3.png','100'),(4,'THE GIFT','4.png','100'),(5,'THE GIFT','5.png','100'),(6,'THE GIFT','6.png','100'),(7,'THE GIFT','7.png','100'),(8,'THE GIFT','8.png','100'),(9,'THE GIFT','9.png','100');
/*!40000 ALTER TABLE `gifts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_objects`
--

DROP TABLE IF EXISTS `payment_objects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_objects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_objects`
--

LOCK TABLES `payment_objects` WRITE;
/*!40000 ALTER TABLE `payment_objects` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment_objects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `amount_label` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'KEYIFCE CEVIZLI SUCU','ədəd'),(2,'PAREX CIFT PASPASLI','ədəd'),(3,'LOTTO ERKEK AYAKKA','ədəd'),(4,'MIR LAKOMSTVO TORT D','ədəd'),(5,'LOTTO ERKEK AYAKKA','ədəd'),(6,'SCHAUMA SAMPUN 225 M','ədəd'),(7,'WELLATON FOAM 9/1 LI','ədəd'),(8,'LOTTO ERKEK T-SHIRT','ədəd'),(9,'KOM MAYO DOKUMA ERKE','ədəd');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-09-15 23:43:11
