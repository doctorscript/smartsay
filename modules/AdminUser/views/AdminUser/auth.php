<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<!-- General meta information -->
	<title>...</title>
	<meta name="robots" content="no-index, no-follow" />
	<meta charset="utf-8" />
	<!-- // General meta information -->
	<!-- Load stylesheets -->
	<link type="text/css" rel="stylesheet" href="<?php echo $this->url()->base()?>/adminmedia/css/auth.css" media="screen" />
	<!-- // Load stylesheets -->	
</head>
<body>

	<div id="wrapper">
		<div id="wrappertop"></div>

		<div id="wrappermiddle">
<?php
if (!empty($errors)) {
	foreach ($errors as $error) {
		echo '<p class="std_error">'.$error.'</p>';
	}
}
?>
			<h2>SmartSay</h2>

			<div id="username_input">

				<div id="username_inputleft"></div>

				<div id="username_inputmiddle">
				<form action="<?php echo $this->url()->toRoute('admin_auth');?>" method="post">
					<input type="text" name="email" id="url" value="<?php echo $this->escapePost('email');?>" autocomplete="off">
					<img id="url_user" src="<?php echo $this->url()->base()?>adminmedia/images/mailicon.png" alt="">
				</div>

				<div id="username_inputright"></div>

			</div>

			<div id="password_input">

				<div id="password_inputleft"></div>

				<div id="password_inputmiddle">
					<input type="password" name="password" id="url" autocomplete="off">
					<img id="url_password" src="<?php echo $this->url()->base();?>adminmedia/images/passicon.png" alt="">
				</div>

				<div id="password_inputright"></div>

			</div>

			<div id="submit">
				<input type="image" src="<?php echo $this->url()->base();?>adminmedia/images/submit.png" id="submit2" value="Sign In">
				</form>
			</div>
		</div>
	</div>

</body>
</html>