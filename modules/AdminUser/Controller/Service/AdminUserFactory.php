<?php
namespace AdminUser\Controller\Service;

use DoctorScript\ServiceManager\ServiceFactoryInterface;
use DoctorScript\ServiceManager\ServiceLocatorInterface;
use AdminUser\Controller\AdminUserController;

class AdminUserFactory implements ServiceFactoryInterface
{
	public function createService(ServiceLocatorInterface $serviceLocator)
	{
		$parentLocator = $serviceLocator->getServiceLocator();
		
		$adminUser = $parentLocator->get('AdminUser\Model\AdminUserInterface');
		$session   = $parentLocator->get('DoctorScript\Session\Storage');

		$controller = new AdminUserController($adminUser, $session);
		return $controller;
	}
}