<?php
return [
	'routes' => [
		'admin_auth' => [
			'pattern' 	 => 'x/_adminzone/?',
			'controller' => 'AdminUser\Controller\AdminUserController',
			'action' 	 => 'auth',
		],
		'admin_logout' => [
			'pattern' 	 => 'x/_adminzone/logout',
			'controller' => 'AdminUser\Controller\AdminUserController',
			'action' 	 => 'logout',
		],
	],
	'controller_manager' => [
		'AdminUser\Controller\AdminUserController' => 'AdminUser\Controller\Service\AdminUserFactory',
	],
	'service_manager' => [
		'AdminUser\Model\AdminUserInterface' => 'AdminUser\Model\Service\AdminUserFactory',
	],
	'view_manager' => [
		'template_map' => [
			'admin/auth' => __DIR__ . '/../views/AdminUser/auth.php',
		],
	],
	'translator' => [
		'paths' => [
			__DIR__ . '/../translates'
		],
	]
];