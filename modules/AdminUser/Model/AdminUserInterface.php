<?php
namespace AdminUser\Model;

interface AdminUserInterface
{
	public function auth($login, $password);
}