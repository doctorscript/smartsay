<?php
namespace AdminUser\Model;

use Common\Model\AbstractModel;

class AdminUser extends AbstractModel implements AdminUserInterface
{	
	public function auth($email, $password)
	{
		$result = $this->getCommon()->selectOne('SELECT `id`, `password` FROM `admin_users` WHERE `email`=:email', [
			':email' => $email,
		]);
		
		if (!$result || !password_verify($password, $result['password'])) {
			return false;
		}
		
		return $result['id'];
	}
}