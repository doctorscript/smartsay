<?php
namespace AdminUser\Model\Service;

use DoctorScript\ServiceManager\ServiceFactoryInterface;
use DoctorScript\ServiceManager\ServiceLocatorInterface;
use AdminUser\Model\AdminUser;

class AdminUserFactory implements ServiceFactoryInterface
{
	public function createService(ServiceLocatorInterface $serviceLocator)
	{
		$common    = $serviceLocator->get('Common\Model\CommonInterface');
		$adminUser = new AdminUser($common);
		
		return $adminUser;
	}
}