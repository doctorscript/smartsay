<?php
namespace AdminUser;

use Core\Mvc\MvcEvent;

class Module
{	
	public function getConfig()
	{
		return require_once(__DIR__ . '/config/module.config.php');
	}
}