<?php
$pages['pages'] = range(1,5);
$pages['next'] = 4;
$pages['last'] = 5;
$pages['prev'] = 2;
$pages['current'] = 3;
if (count($pages['pages']) > 1) {
$pagination = '';
$currentUrl = $this->url()->toRoute('client_purchases', [
	'lang'   => $this->translate()->getLocale(),
	'market' => 'megastore'
]); 
$currentUrl .= '?client_id='.$this->escapeGet('client_id');
?>
<nav>
  <ul class="pagination">
	<?php
		if (!empty($pages['prev'])) {
			$prev  = '<li><a href="'.$currentUrl.'&page=1" title="Geri">&lsaquo;</a></li>';
			$prev .= '<li><a href="'.$currentUrl.'&page='.(int)$pages['prev'].'" aria-label="Previous">';
			$prev .= '<span aria-hidden="true">&laquo;</span></a></li>';
			echo $prev;
		}

		foreach ($pages['pages'] as $page) {
			$page = (int)$page;
			$active = $page == $pages['current'] ? 'class="active"' : '';
			echo '<li '.$active.'><a href="'.$currentUrl.'&page='.$page.'">'.$page.'</a></li>';
		}
		
		if (!empty($pages['next'])) {
			$next  = '<li><a href="'.$currentUrl.'&page='.(int)$pages['next'].'" title="İrəli">&rsaquo;</a></li>';
			$next .= '<li><a href="'.$currentUrl.'&page='.(int)$pages['last'].'" aria-label="Next">';
			$next .= '<span aria-hidden="true">&raquo;</span></a></li>';
			echo $next;
		}
	?>
  </ul>
</nav>
<?php
}
?>