<!doctype html>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>SmartSay Admin panel</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" type="text/css" href="<?php echo $this->url()->base()?>adminmedia/lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo $this->url()->base()?>adminmedia/lib/font-awesome/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $this->url()->base()?>adminmedia/css/theme.css">

    <script src="<?php echo $this->url()->base()?>/adminmedia/lib/jquery-1.11.1.min.js" type="text/javascript"></script>
	<script src="<?php echo $this->url()->base()?>/adminmedia/lib/bootstrap/js/bootstrap.js"></script>

</head>
<body class="theme-blue">
    <div class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
          <a class="" href="index.html"><span class="navbar-brand">SmartSay</span></a></div>

        <div class="navbar-collapse collapse" style="height: 1px;">
          <a href="<?php echo $this->url()->toRoute('admin_logout')?>" style="position:absolute;top: 15px;right:10px;color:#fff">
             <span class="padding-right-small"></span> Выйти
          </a>
        </div>
      </div>
    </div>
    <div class="sidebar-nav">
    <ul>
    <li><a href="#" data-target=".dashboard-menu" class="nav-header" data-toggle="collapse"><i class="fa fa-fw fa-dashboard"></i>Пользователи системы<i class="fa fa-collapse"></i></a></li>
    <li><ul class="dashboard-menu nav nav-list collapse in">
            <li class="active"><a href="<?php echo $this->url()->toRoute('admin_system_users');?>"><span class="fa fa-caret-right"></span>Список</a></li>
    </ul></li>
    </div>

<?php
	echo $this->renderChild('content');
?>
</body>
</html>
