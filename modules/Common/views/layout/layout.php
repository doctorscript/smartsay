<!DOCTYPE html>
<html lang="<?php echo $this->translate()->getLocale();?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>SmartSay.az</title>

    <!-- Bootstrap -->
    <link href="<?php echo $this->url()->base()?>media/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $this->url()->base()?>media/css/jquery-ui.min.css" rel="stylesheet">
    <link href="<?php echo $this->url()->base()?>media/css/style.css" rel="stylesheet">
    <!--<link href="css/style.css" rel="stylesheet">-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="<?php echo $this->url()->base()?>media/js/html5shiv.min.js"></script>
    <script src="<?php echo $this->url()->base()?>media/js/respond.min.js"></script>
    <![endif]-->
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="<?php echo $this->url()->base()?>media/js/jquery-1.11.3.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="<?php echo $this->url()->base()?>media/js/bootstrap.min.js"></script>
	<script src="<?php echo $this->url()->base()?>media/js/jquery.bpopup.min.js"></script>
	<script src="<?php echo $this->url()->base()?>media/js/script.js"></script>
</head>
<body>

<!--Top menu start-->
<nav class="navbar navbar-green navbar-static-top" id="main_navbar">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
			<?php
				$ru = htmlspecialchars(str_replace(['/ru', '/az'], 'ru', $_SERVER['REQUEST_URI']));
				$az = htmlspecialchars(str_replace(['/ru', '/az'], 'az', $_SERVER['REQUEST_URI']));
			?>
			<script>
				$(document).ready(function(){
					$('#change_lang').on('change', function(){
						location.href = $(this).val();
					});
				});
			</script>
			<select name="lang" class="navbar-select" id="change_lang">
                <?php 
					$selectedAz = $this->translate()->getLocale() == 'az' ? 'selected="selected"' : '';
					$selectedRu = $this->translate()->getLocale() == 'ru' ? 'selected="selected"' : '';
				?>
				<option value="<?php echo $this->url()->base().$az;?>" <?php echo $selectedAz;?>>Azərbaycan dilində</option>
                <option value="<?php echo $this->url()->base().$ru;?>" <?php echo $selectedRu;?>>Русский язык</option>
            </select>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <?php
				if ($routeName != 'registration') {
					$lang = $this->translate()->getLocale();
					$navs = [[
						'name'   => $this->translate('markets'),
						'params' => ['lang' => $lang],
						'route'  => 'market_list'
					],/*  [
						'name'   => 'Hədiyyələr',
						'params' => ['lang' => $lang, 'market' => 'megastore'],
						'route'  => 'gifts_list',
					], */[
						'name'   => $this->translate('personal_information'),
						'params' => ['lang' => $lang],
						'route'  => 'edit_user_data',
					], /* [
						'name'   => 'Çıxış',
						'params' => ['lang' => $lang],
						'route'  => 'system_user_logout',
					] */];
			?>
			<ul class="nav navbar-nav navbar-right">
                <?php
					foreach ($navs as $nav) {
						$active = $nav['route'] == $routeName ? 'class="active"' : '';
						echo '<li '.$active.'><a href="'.$this->url()->toRoute($nav['route'], $nav['params']).'">'.$nav['name'].'</a></li>';
					}
					if (!empty($systemUserClients)) {
				?>
				
				<li class="dropdown">
				  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $this->translate('my_markets');?> <span class="caret"></span></a>
				  <ul class="dropdown-menu">
					<?php
						foreach ($systemUserClients as $systemUserClient) {
							$clientDescription  = $this->translate('market_'.$systemUserClient['target_database']) . ' - ';
							$clientDescription .= htmlspecialchars($systemUserClient['name']) . ' '; 
							$clientDescription .= htmlspecialchars($systemUserClient['surname']); 
							$url = $this->url()->toRoute('client_purchases', [
								'lang'   => $this->translate()->getLocale(),
								'market' => $systemUserClient['target_database']
							]);
							$url .= '?client_id='.$systemUserClient['id'];
							echo '<li><a href="'.$url.'">'.$clientDescription.'</a></li>';
						} 
					?>
				  </ul>
				</li>
				<?php
					}
				?>
				<li><a href="<?php echo $this->url()->toRoute('system_user_logout', ['lang' => $lang]);?>?csrf=<?php echo $this->csrfToken;?>"><?php echo $this->translate('exit');?></a></li>
            </ul>
			<?php
				}
			?>
        </div>
        <!--/.nav-collapse -->
    </div>
</nav>
<!--top menu end-->

<div class="container" id="wrapper">
    <div class="row">
        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
            <!--Header start-->
            <div class="hidden-xs">
                <a href="<?php echo $this->url()->base()?>"><img src="<?php echo $this->url()->base()?>media/img/logo1.png" alt="SmartSay logo"/></a>
            </div>
            <!--Header   end-->


            <!--This Header will shows only on mobile phones-->
            <div style="text-align: center;" class="visible-xs">
                <a href="#"><img src="<?php echo $this->url()->base()?>media/img/logo1.png" alt="SmartSay logo"/></a>
            </div>
            <!--Header   end-->
        </div>
    </div>
    <div class="header_line"></div>
	 <div class="row">
        <div class="col-lg-12">
            <!--Body start-->
            <div id="body" class="inner_container centered" style="padding-top: 10px;">
				<?php echo $this->renderChild('content');?>
				
			<div class="row">
				<div class="col-lg-12">
					<!--Social icons start-->
					<div id="social_icons">
						<ul>
							<li><a href="#"><img src="<?php echo $this->url()->base()?>media/img/social_icons/Facebook.png" alt="Facebook" height="20"/></a></li>
							<li><a href="#"><img src="<?php echo $this->url()->base()?>media/img/social_icons/Twitter.png" alt="Twitter"/></a></li>
							<li><a href="#"><img src="<?php echo $this->url()->base()?>media/img/social_icons/Youtube.png" alt="Youtube" height="20"/></a></li>
							<li><a href="#"><img src="<?php echo $this->url()->base()?>media/img/social_icons/Apple.png" alt="AppStore" height="22"/></a></li>
							<li><a href="#"><img src="<?php echo $this->url()->base()?>media/img/social_icons/Android.png" alt="PlayMarket" height="22"/></a></li>
						</ul>
					</div>
					<!--Social icons   end-->
					<div id="copyright">
						<p>&copy; <?php echo date('Y');?> SmartSay.az</p>
						<p style="text-align:center;color:#A9ABAE;"><?php echo $this->translate('for_additional_info');?>: <a style="color:#64A039;" href="mailto:info@smartsay.az">info@smartsay.az</a></p>
					</div>
				</div>
			</div>
			</div>
            <!--Body   end-->
        </div>
    </div>
</div>
</body>
</html>