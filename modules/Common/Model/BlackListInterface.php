<?php
namespace Common\Model;

interface BlackListInterface
{
	public function isBanned($ip, $email, $where);
	public function eraseOld();
	public function add($ip, $email, $where);
}