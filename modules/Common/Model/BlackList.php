<?php
namespace Common\Model;

use Common\Model\AbstractModel;

class BlackList extends AbstractModel implements BlackListInterface
{
	private $blackListTable = 'black_list';
	
	public function isBanned($ip, $email, $where)
	{
		$this->eraseOld();
		$common = $this->getCommon();
		$result = $common->selectOne('SELECT COUNT(1) AS cnt
										FROM '.$this->blackListTable.' 
										   WHERE `ip`=:ip
										   AND `email`=:email
										   AND `where`=:where
										GROUP BY `ip`
										HAVING cnt >= 3', [':ip' => $ip, ':email' => $email, ':where' => $where]);
		return $result ? true : false;
	}
	
	public function eraseOld()
	{
		$common = $this->getCommon();
		return $common->query('DELETE FROM '.$this->blackListTable.' WHERE `datetime` <= (NOW() - INTERVAL 15 MINUTE)');
	}
	
	public function add($ip, $email, $where)
	{
		$common = $this->getCommon();
		return $common->query('INSERT INTO `'.$this->blackListTable.'` (`ip`, `email`, `where`, `datetime`) VALUES (:ip, :email, :where, NOW())', [':ip' => $ip, ':email' => $email, ':where' => $where]);
	}
}