<?php
namespace Common\Model;

use DoctorScript\ServiceManager\ServiceLocatorInterface;
use DoctorScript\ServiceManager\ServiceLocatorAwareInterface;

class Common implements CommonInterface, ServiceLocatorAwareInterface
{
	protected $dbh;
	private $serviceLocator;
	
	public function __construct(\PDO $dbh)
	{
		$this->dbh = $dbh;
	}
	
	public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
	{
		$this->serviceLocator = $serviceLocator;
	}
	
	public function getServiceLocator()
	{
		return $this->serviceLocator;
	}
	
	public function getDbh()
	{
		return $this->dbh;
	}
	
	public function lastInsertId()
	{
		return $this->dbh->lastInsertId();
	}
	
	public function insert($table, array $data)
	{
		$columns = $placeholders = '';
		foreach (array_keys($data) as $value) {
			$columns 	  .= '`'.$value.'`,';
			$placeholders .= ':'.$value.',';
			$data[':'.$value] = $data[$value];
			unset($data[$value]);
		}

		$columns 	  = rtrim($columns,',');
		$placeholders = rtrim($placeholders,',');

		$sql = 'INSERT INTO `'.$table.'` ('.$columns.') VALUES ('.$placeholders.')';
		$stmt = $this->dbh->prepare($sql);
		return $stmt->execute($data);
	}
	
	public function update($table, array $data, $where)
	{
		$sql = 'UPDATE `'.$table.'` SET ';
		
		foreach (array_keys($data) as $value) {
			$sql .= '`'.$value.'` = :'.$value.',';
		}
		
		$sql = rtrim($sql, ',') .' '. $where;

		$stmt = $this->dbh->prepare($sql);
		return $stmt->execute($data);
	}
	
	public function select($sql, array $data = [], $fetchMode = \PDO::FETCH_ASSOC)
	{
		$stmt = $this->dbh->prepare($sql);
		$stmt->execute($data);
		
		return $stmt->fetchAll($fetchMode);
	}
	
	public function selectOne($sql, array $data = [], $fetchMode = \PDO::FETCH_ASSOC)
	{
		$stmt = $this->dbh->prepare($sql . ' LIMIT 1');
		$stmt->execute($data);
		
		return $stmt->fetch($fetchMode);
	}
	
	public function exists($table, $column, $value)
	{
		$count = $this->selectOne('SELECT COUNT(1) AS `value` FROM `'.$table.'` WHERE `'.$column.'` = :value', [':value' => $value]);
		if (!$count) {
			return false;
		}
		
		return $count['value'] > 0;
	}
	
	public function count($table)
	{
		$result = $this->dbh->query('SELECT COUNT(1) AS `cnt` FROM `'.$table.'`')->fetch(\PDO::FETCH_ASSOC);
		return $result['cnt'];
	}
	
	public function query($sql, array $data = [])
	{
		$stmt = $this->dbh->prepare($sql);
		return $stmt->execute($data);
	}
}