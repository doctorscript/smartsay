<?php
namespace Common\Model\Service;

use DoctorScript\ServiceManager\ServiceFactoryInterface;
use DoctorScript\ServiceManager\ServiceLocatorInterface;
use Common\Model\BlackList;

class BlackListFactory implements ServiceFactoryInterface
{
	public function createService(ServiceLocatorInterface $serviceLocator)
	{
		$blackList = new BlackList($serviceLocator->get('Common\Model\CommonInterface'));
		return $blackList;
	}
}