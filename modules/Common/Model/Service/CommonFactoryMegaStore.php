<?php
namespace Common\Model\Service;

use DoctorScript\ServiceManager\ServiceFactoryInterface;
use DoctorScript\ServiceManager\ServiceLocatorInterface;
use Common\Model\Common;

class CommonFactoryMegaStore implements ServiceFactoryInterface
{
	public function createService(ServiceLocatorInterface $serviceLocator)
	{
		$config = $serviceLocator->get('config')['pdo_megastore'];
		$pdo = new \PDO($config['driver'].':host='.$config['host'].';dbname='.$config['dbname'].';charset=utf8', $config['user'], $config['password'], $config['options']);
		
		return new Common($pdo);
	}
}