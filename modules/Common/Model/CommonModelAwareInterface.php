<?php
namespace Common\Model;

interface CommonModelAwareInterface
{
	public function setCommon(CommonInterface $common);
	public function getCommon();
}