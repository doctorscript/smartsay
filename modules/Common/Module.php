<?php
namespace Common;

use DoctorScript\Mvc\MvcEvent;
use DoctorScript\ServiceManager\ServiceLocatorInterface;
use DoctorScript\ServiceManager\RouteMatchInterface;

class Module
{
	public function init(MvcEvent $e)
	{
		$app = $e->getApplication();
		$sm  = $app->getServiceManager();
		$em  = $app->getEventManager();
		$em->attach($e::EVENT_FAILED_REQUEST, function($e) use ($em, $sm, $app){
			$match = $e->getRouteMatch();
			if (is_null($match)) {
				$match = new \DoctorScript\Router\RouteMatch();
				$match->setParam('controller', 'Common\Controller\ErrorHandlerController');
				$match->setParam('action', 'notFound');
				$e->setRouteMatch($match);
				$em->trigger($e::EVENT_ROUTE, $app);
			} else {
				$match->setParam('controller', 'Common\Controller\ErrorHandlerController');
				$match->setParam('action', 'notFound');
			}
		});
		
		$em->attach($e::EVENT_ROUTE, function($e) use ($em, $sm, $app){
			$market = $e->getRouteMatch()->getParam('market');
			if (!empty($market)) {
				$markets = $sm->get('config')['markets'];
				if (!in_array($market, $markets)) {
					$em->trigger($e::EVENT_FAILED_REQUEST, $app);
				}
			}
		}, -2001);
		
		$view = $e->getView();
		$view->setTemplate('layout');

		$events = [
			'Market\Controller\MarketController', 
			'Gifts\Controller\GiftsController',
			'SystemUser\Controller\SystemUserController'
		];
		
		$em->attach($events, function($e) use ($sm, $view, $app){
			$match = $e->getRouteMatch();
			if ($e->getName() == 'SystemUser\Controller\SystemUserController' ) {
				$action = $match->getParam('action');
				if (in_array($action, ['register', 'auth', 'recover', 'captcha'])) {
					return;
				}
			}
			$session = $sm->get('DoctorScript\Session\Storage');
			$request = $sm->get('DoctorScript\Http\Request\RequestInterface');
			if ($session->has('system_user_id')) {
				if (!$request->isXhr()) {
					$systemUser = $sm->get('SystemUser\Model\SystemUser');
					$clients	= $systemUser->getClients($session->get('system_user_id'));
					$view->addVariable('systemUserClients', $clients);
				}
			} else {
				if ($this->needRedirectToAuth($e)) {
					$this->redirectToAuth($sm);
				}
			}
		});

		$em->attach($e::EVENT_ROUTE, function($e) use ($sm, $view, $em, $app) {
			$session = $sm->get('DoctorScript\Session\Storage');
			$request = $sm->get('DoctorScript\Http\Request\RequestInterface');
			//restore access to account if user is remembered
			if (!$session->has('system_user_id')) {
				if (!isset($_COOKIE['remember_me']) || !is_string($_COOKIE['remember_me'])) {
					if ($request->fromGet('format') == 'json' && $this->needRedirectToAuth($e)) {
						header('Content-Type: application/json');
						http_response_code(403);
						exit;
					} else {
						
						if ($this->needRedirectToAuth($e)) {
							$this->redirectToAuth($sm);
						}
					}
				} else {
					$systemUser = $sm->get('SystemUser\Model\SystemUser');
					if ($systemUserId = $systemUser->getRememberedId($_COOKIE['remember_me'])) {
						$session->set('system_user_id', $systemUserId);
						$csrfHash = substr(password_hash(time(), PASSWORD_BCRYPT), 8, 55);
						$session->set('csrf', $csrfHash);
						if ($request->fromGet('format') == 'json') {
							$session->regenerate();
							header('Content-Type: application/json');
							http_response_code(401);
							echo json_encode([
								'error'   => 'Unauthorized',
								'code'    => 401,
								'session' => 'sessid='.session_id(),
								'csrf'	  => $csrfHash
							]);
							exit;
						}
					} else {
						if ($request->fromGet('format') == 'json' && $this->needRedirectToAuth($e)) {
							header('Content-Type: application/json');
							http_response_code(403);
							exit;
						} else if ($this->needRedirectToAuth($e)) {
							$this->redirectToAuth($sm);
						}
					}
				}
			} 

			if ($session->has('csrf')) {
				$view->addVariable('csrfToken', $session->get('csrf'));
			}
			
			$match = $e->getRouteMatch();
			$view->addVariable('routeName', $match->getParam('route'));
			$translator = $sm->get('translate');
			$translator->setLocale($match->getParam('lang', 'az'));
		}, -2000);
	}
	
	private function redirectToAuth(ServiceLocatorInterface $sm)
	{
		$application = $sm->get('DoctorScript\Mvc\ApplicationInterface');
		$response	 = $sm->get('DoctorScript\Http\Response\ResponseInterface');
		$match		 = $sm->get('DoctorScript\Router\RouterInterface')->getRouteMatch();
		$lang		 = $match->getParam('lang') == 'ru' ? 'ru' : 'az';
		$response->addHeader('Location', '/'.$lang);
		$e = $sm->get('DoctorScript\Mvc\Service\MvcEventFactory');
		$application->completeRequest($e);
	}
	
	private function needRedirectToAuth(MvcEvent $e)
	{
		$match = $e->getRouteMatch();
		$controller = $match->getParam('controller');
		$isUserController = ($controller == 'SystemUser\Controller\SystemUserController');
		$action	= $match->getParam('action');
		if ($isUserController && in_array($action, ['register', 'auth', 'recover', 'captcha'])) {
			return false;
		}
		if ($controller == 'Common\Controller\ErrorHandlerController') {
			return false;
		}
		
		return true;
	}
	
	public function getConfig()
	{
		return require_once(__DIR__ . '/config/module.config.php');
	}
}