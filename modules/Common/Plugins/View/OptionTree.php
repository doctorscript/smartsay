<?php
namespace Common\Plugins\View;

use DoctorScript\Mvc\View\Plugin\MarkerInterface;

class OptionTree implements MarkerInterface
{
	private function buildTree(Array $data, $parent = 0) {
		$tree = array();
		foreach ($data as $d) {
			if ($d['parent_id'] == $parent) {
				$children = $this->buildTree($data, $d['id']);
				if (!empty($children)) {
					$d['_children'] = $children;
				}
				$tree[] = $d;
			}
		}
		return $tree;
	}
	
	private function printTree($tree, $r = 0, $p = null, $selectedValue) {
		static $html;
		foreach ($tree as $i => $t) {
			$dash     = ($t['parent_id'] == 0) ? '' : str_repeat('-', $r) .' ';
			$selected = !empty($selectedValue) && $selectedValue == $t['id'] ? 'selected="selected"' : '';
			$html 	 .= sprintf('<option value="%d" '.$selected.'>%s%s</option>', $t['id'], $dash, $t['header']);
			if ($t['parent_id'] == $p) {
				$r = 0;
			}
			if (isset($t['_children'])) {
				$this->printTree($t['_children'], ++$r, $t['parent_id'], $selectedValue);
			}
		}

		return $html;
	}
	
	public function __invoke(array $data, $selected = null)
	{
		$tree = $this->buildTree($data);
		return $this->printTree($tree, 0, null, $selected);
	}
}