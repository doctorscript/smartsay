<?php
return [
	'markets' => [
		'bazarstore'
	],
	'view_manager' => [
		'template_map' => [
			'layout' => __DIR__ . '/../views/layout/layout.php',
			'error/404' => __DIR__ . '/../views/layout/404.php',
			'layouts/admin' => __DIR__ . '/../views/layout/adminlayout.php',
			'frontend/pagination' => __DIR__ . '/../views/pagination.php',
		],
	],
	'pdo_system' => [
		'driver'   => 'mysql',
		'host'	   => 'localhost',
		'dbname'   => 'ss_system',
		'user'	   => 'root',
		'password' => 'jZ2k7AbE27c3bZ',
		'options'  => [
			PDO::ATTR_ERRMODE 		   => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_EMULATE_PREPARES => false,
		],
	],
	'pdo_megastore' => [
		'driver'   => 'mysql',
		'host'	   => 'localhost',
		'dbname'   => 'market_megastore',
		'user'	   => 'root',
		'password' => 'jZ2k7AbE27c3bZ',
		'options'  => [
			PDO::ATTR_ERRMODE 		   => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_EMULATE_PREPARES => false,
		],
	],
	'service_manager' => [
		'Common\Service\CaptchaFactory' 		   => 'Common\Service\CaptchaFactory',
		'Common\Model\CommonInterface'  		   => 'Common\Model\Service\CommonFactory',
		'Common\Model\CommonInterfaceMegaStore'    => 'Common\Model\Service\CommonFactoryMegaStore',
		'Common\Model\BlackListInterface' 		   => 'Common\Model\Service\BlackListFactory',
	],
	'controller_manager' => [
		'Common\Controller\ErrorHandlerController' => 'Common\Controller\ErrorHandlerController',
	],
	'translator' => [
		'paths' => [
			__DIR__ . '/../translates'
		],
	],
	'baseUrl' => 'https://smartsay.az/',
];