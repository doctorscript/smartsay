-- phpMyAdmin SQL Dump
-- version 4.5.3.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 21, 2016 at 12:16 PM
-- Server version: 5.7.10-log
-- PHP Version: 7.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `megastore`
--

-- --------------------------------------------------------

--
-- Table structure for table `client_purchases`
--

CREATE TABLE `client_purchases` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `cheque_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `amount` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `client_purchases`
--

INSERT INTO `client_purchases` (`id`, `product_id`, `cheque_id`, `price`, `amount`) VALUES
(1, 1, 1, '4.09', '1'),
(2, 2, 1, '10.30', '1'),
(3, 3, 2, '23.99', '1'),
(4, 4, 2, '1.19', '1'),
(5, 3, 3, '19.99', '1'),
(6, 4, 3, '1.19', '1'),
(7, 6, 4, '1.05', '1'),
(8, 7, 4, '6.85', '1'),
(9, 3, 4, '19.99', '1'),
(10, 8, 5, '14.95', '1'),
(11, 9, 5, '26.86', '1'),
(12, 9, 6, '26.86', '1'),
(13, 4, 7, '1.19', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `client_purchases`
--
ALTER TABLE `client_purchases`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `client_purchases`
--
ALTER TABLE `client_purchases`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
