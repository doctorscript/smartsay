<?php
return [
	//errors
	'incorrect_captcha' 	    => 'Təhlükəsizlik kodu düzgün daxil olunmayıb',
	'invalid_login_or_password' => 'Login və ya şifrə düzgün daxil olunmayıb',
	'invalid_email_or_password' => 'E-mail və ya şifrə düzgün daxil olunmayıb',
	'temporary_banned_msg'		=> '15 dəqiqədən sonra birdə cəhd edin',
	
	//labels
	'try_login' => 'Daxil ol',
	'for_additional_info' => 'Əlavə məlumat üçün',
	'search' => 'Axtar',
	'please_wait' => 'Zəhmət olmasa gözləyin',
	
	//menu
	'markets' => 'Marketlər',
	'personal_information' => 'Şəxsi məlumatlar',
	'my_markets' => 'Mənim marketlərim',
	'exit' => 'Çıxış',
];