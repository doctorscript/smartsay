<?php
return [
	'routes' => [
		'market_list' => [
			'pattern' 	 => '(?<lang>az|ru)/market/list/?',
			'controller' => 'Market\Controller\MarketController',
			'action' 	 => 'list',
		],
		'client_auth' => [
			'pattern' 	 => '(?<lang>az|ru)/market/(?<market>[\w]+)/clientauth/?',
			'controller' => 'Market\Controller\MarketController',
			'action' 	 => 'clientAuth',
		],
		'client_search'  => [
			'pattern' 	 => '(?<lang>az|ru)/market/(?<market>[\w]+)/clientsearch/?',
			'controller' => 'Market\Controller\MarketController',
			'action' 	 => 'clientSearch',
		],
		'client_purchases' => [
			'pattern' 	 => '(?<lang>az|ru)/market/(?<market>[\w]+)/purchases/?',
			'controller' => 'Market\Controller\MarketController',
			'action' 	 => 'purchases',
		],
		'market_shares' => [
			'pattern' 	 => '(?<lang>az|ru)/market/(?<market>[\w]+)/shares/?',
			'controller' => 'Market\Controller\MarketController',
			'action' 	 => 'shares',
		],
	],
	'controller_manager' => [
		'Market\Controller\MarketController' => 'Market\Controller\Service\MarketFactory',
	],
	'service_manager' => [
		'Market\Model\MarketInterface' => 'Market\Model\Service\MegaStoreAdapterFactory',
		'Market\Model\Adapter\MegaStore' => 'Market\Model\Service\MegaStoreAdapterFactory',
		'Market\Validator\ClientCard\ValidatorInterface' => 'Market\Validator\Service\ClientCardFactory',
	],
	'translator' => [
		'paths' => [
			__DIR__ . '/../translates'
		],
	],
	'markets' => [
		'megastore' => 'BazarStore',
	],
];