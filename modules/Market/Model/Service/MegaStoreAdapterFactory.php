<?php
namespace Market\Model\Service;

use DoctorScript\ServiceManager\ServiceFactoryInterface;
use DoctorScript\ServiceManager\ServiceLocatorInterface;
use Market\Model\Adapter\MegaStore;

class MegaStoreAdapterFactory implements ServiceFactoryInterface
{
	public function createService(ServiceLocatorInterface $serviceLocator)
	{		
		$common    = $serviceLocator->get('Common\Model\CommonInterfaceMegaStore');
		$megaStore = new MegaStore($common);

		return $megaStore;
	}
}