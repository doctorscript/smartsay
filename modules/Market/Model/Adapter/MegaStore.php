<?php
namespace Market\Model\Adapter;

use Common\Model\AbstractModel;
use Market\Model\MarketInterface;

class MegaStore extends AbstractModel implements MarketInterface
{
	const CARD_IMAGE = 'bazarstore.png';
	
	public function getClients(array $id) : array
	{
		$placeholders = rtrim(str_repeat('?,', count($id)), ',');
		$sql = 'SELECT clients.id, clients.name, clients.surname, "bazarstore" AS target_database
					FROM clients
				WHERE id IN('.$placeholders.')';

		return $this->getCommon()->select($sql, $id);
	}
	
	public function getClient($cardNumber)
	{
		$sql = 'SELECT name, surname 
					FROM clients
				WHERE card_number=CAST(:card_number AS UNSIGNED)';
		
		$result = $this->getCommon()->selectOne($sql, [
			':card_number'  => $cardNumber,
		]);
		
		if ($result) {
			$result['card_image'] = self::CARD_IMAGE;
		}
		
		return $result;
	}
	
	public function getClientId($cardNumber, $phoneNumber)
	{
		$sql = 'SELECT id 
					FROM clients
				WHERE card_number=CAST(:card_number AS UNSIGNED)
				AND phone_number=:phone_number';
		
		$result = $this->getCommon()->selectOne($sql, [
			':card_number'  => $cardNumber,
			':phone_number' => $phoneNumber,
		]);

		return isset($result['id']) ? $result['id'] : false;
	}
	
	private function getChequesOrder($order = null) : string
	{
		$orderSql = 'cheques.datetime DESC';
		if (!empty($order)) {
			switch($order) 
			{
				case 'much': 	$orderSql = 'cheque_sum DESC';break;
				case 'few': 	$orderSql = 'cheque_sum ASC';break;
				case 'near':	$orderSql = 'cheques.datetime DESC';break;
				case 'distant': $orderSql = 'cheques.datetime ASC';break;
			}
		}
		
		return $orderSql;
	}
	
	public function getCheques($clientId, $offset, $limit, $order = null)
	{
		$orderSql = $this->getChequesOrder($order);
		
		$sql = 'SELECT cheques.id, 
					   DATE_FORMAT(cheques.datetime, "%d.%m.%Y %H:%i") AS purchases_datetime, cheques.bonus, 
					   SUM(client_purchases.price * client_purchases.amount) AS cheque_sum
					FROM cheques
						JOIN client_purchases ON client_purchases.cheque_id = cheques.id
						JOIN products ON products.id = client_purchases.product_id
						JOIN clients ON clients.id = cheques.client_id
					WHERE clients.id=:client_id
					GROUP BY cheques.id
					ORDER BY '.$orderSql.' 
					LIMIT :offset, :limit';

		return $this->getCommon()->select($sql, [
			':client_id' 	  => $clientId,
			':offset' 	 	  => $offset,
			':limit' 	 	  => $limit
		]);
	}
	
	public function getChequesByYear($clientId, $offset, $limit, $order = null)
	{
		$orderSql = $this->getChequesOrder($order);
		$sql = 'SELECT cheques.id, SUM(client_purchases.price * client_purchases.amount) AS cheque_sum,
					   tmp_cheques.bonus,
					   YEAR (cheques.datetime) AS purchases_datetime
							FROM cheques
								JOIN client_purchases ON client_purchases.cheque_id = cheques.id
								JOIN products ON products.id = client_purchases.product_id
								JOIN clients ON clients.id = cheques.client_id
								LEFT JOIN (
									SELECT SUM(bonus) AS bonus, YEAR (cheques.datetime) AS y 
										FROM cheques 
									GROUP BY y
								) AS tmp_cheques ON tmp_cheques.y = YEAR(cheques.datetime)
						WHERE clients.id=:client_id
						GROUP BY purchases_datetime
						ORDER BY '.$orderSql.'
						LIMIT :offset, :limit';
		
		return $this->getCommon()->select($sql, [
			':client_id' 	  => $clientId,
			':offset' 	 	  => $offset,
			':limit' 	 	  => $limit
		]);
	}
	
	public function getChequesByMonth($clientId, $offset, $limit, $order = null)
	{
		//CONCAT_WS (".", YEAR(cheques.datetime), MONTH(cheques.datetime)) AS purchases_datetime
		$orderSql = $this->getChequesOrder($order);
		$sql = 'SELECT cheques.id, SUM(client_purchases.price * client_purchases.amount) AS cheque_sum,
					   tmp_cheques.bonus,
					   YEAR (cheques.datetime) AS purchases_year,
					   MONTH (cheques.datetime) AS purchases_month,
					   DATE_FORMAT(cheques.datetime, "%Y.%m") AS purchases_datetime
							FROM cheques
								JOIN client_purchases ON client_purchases.cheque_id = cheques.id
								JOIN products ON products.id = client_purchases.product_id
								JOIN clients ON clients.id = cheques.client_id
								LEFT JOIN (
									SELECT SUM(bonus) AS bonus, YEAR (cheques.datetime) AS y, MONTH (cheques.datetime) AS m
										FROM cheques 
									GROUP BY y, m
								) AS tmp_cheques ON tmp_cheques.y = YEAR(cheques.datetime) 
													AND tmp_cheques.m = MONTH(cheques.datetime)
						WHERE clients.id=:client_id
						GROUP BY purchases_year, purchases_month
						ORDER BY '.$orderSql.'
						LIMIT :offset, :limit';
		
		return $this->getCommon()->select($sql, [
			':client_id' 	  => $clientId,
			':offset' 	 	  => $offset,
			':limit' 	 	  => $limit
		]);
	}
	
	public function getChequesCountByMonth($clientId, array $yearMonths)
	{
		$placeholders = rtrim(str_repeat('?,', count($yearMonths)), ',');
		$params		  = array_merge([$clientId], $yearMonths);

		$sql = 'SELECT COUNT(DISTINCT DATE_FORMAT(cheques.datetime, "%m-%Y")) AS cnt
					FROM cheques
						JOIN client_purchases ON client_purchases.cheque_id = cheques.id
						JOIN products ON products.id = client_purchases.product_id
						JOIN clients ON clients.id = cheques.client_id
					WHERE clients.id=?
					AND DATE_FORMAT(cheques.datetime, "%Y.%m") IN ('.$placeholders.')';
		$result = $this->getCommon()->selectOne($sql, $params);
		return $result ? $result['cnt'] : 0;
	}
	
	public function getChequePurchasesByMonth($clientId, array $yearMonths)
	{

		$placeholders = rtrim(str_repeat('?,', count($yearMonths)), ',');
		$params		  = array_merge([$clientId], $yearMonths);

		$sql = 'SELECT cheques.id, SUM(client_purchases.price * client_purchases.amount) AS price,
					   client_purchases.price AS product_price,
					   (SUM(cheques.bonus) * COUNT(DISTINCT cheques.id) / COUNT(*)) AS bonus,
					   YEAR (cheques.datetime) AS purchases_year,
					   MONTH (cheques.datetime) AS purchases_month,
					   DATE_FORMAT(cheques.datetime, "%Y.%m") AS purchases_datetime,
					   products.name,
					   products.amount_label,
					   SUM(client_purchases.amount) AS amount
							FROM cheques
								JOIN client_purchases ON client_purchases.cheque_id = cheques.id
								JOIN products ON products.id = client_purchases.product_id
								JOIN clients ON clients.id = cheques.client_id
							WHERE clients.id=?
							AND DATE_FORMAT(cheques.datetime, "%Y.%m") IN ('.$placeholders.')
						GROUP BY products.id, purchases_datetime
						ORDER BY cheques.id DESC';
		
		return $this->getCommon()->select($sql, $params);
	}
	
	public function getChequesCountByYear($clientId, array $years)
	{
		$placeholders = rtrim(str_repeat('?,', count($years)), ',');
		$params		  = array_merge([$clientId], $years);

		$sql = 'SELECT COUNT(DISTINCT YEAR(cheques.datetime)) AS cnt
					FROM cheques
						JOIN client_purchases ON client_purchases.cheque_id = cheques.id
						JOIN products ON products.id = client_purchases.product_id
						JOIN clients ON clients.id = cheques.client_id
					WHERE clients.id=?
					AND YEAR(cheques.datetime) IN ('.$placeholders.')';
		
		$result = $this->getCommon()->selectOne($sql, $params);
		return $result ? $result['cnt'] : 0;
	}
	
	public function getChequePurchasesByYear($clientId, array $years)
	{
		$placeholders = rtrim(str_repeat('?,', count($years)), ',');
		$params		  = array_merge([$clientId], $years);
		
		$sql = 'SELECT cheques.id AS cheque_id, 
					   products.name, 
					   SUM(client_purchases.price * client_purchases.amount) AS price, 
					   client_purchases.price AS product_price,
					   SUM(client_purchases.amount) AS amount, 
					   YEAR (cheques.datetime) AS purchases_datetime,
					   products.amount_label
							FROM products
								JOIN client_purchases ON client_purchases.product_id = products.id
								JOIN cheques ON cheques.id = client_purchases.cheque_id
								JOIN clients ON clients.id = cheques.client_id
							WHERE clients.id=?
							AND YEAR(cheques.datetime) IN ('.$placeholders.')
							GROUP BY products.id, purchases_datetime
							ORDER BY cheque_id DESC';

		return $this->getCommon()->select($sql, $params);
	}
	
	public function getChequesCount($clientId)
	{
		$sql = 'SELECT COUNT(DISTINCT cheques.id) AS cnt
					FROM cheques
						JOIN client_purchases ON client_purchases.cheque_id = cheques.id
						JOIN products ON products.id = client_purchases.product_id
						JOIN clients ON clients.id = cheques.client_id
					WHERE clients.id=:client_id';
		
		$result = $this->getCommon()->selectOne($sql, [
			':client_id' 	  => $clientId,
		]);
		
		return $result ? $result['cnt'] : 0;
	}

	public function getChequePurchases(array $cheques)
	{
		$placeholders = rtrim(str_repeat('?,', count($cheques)), ',');
		
		$sql = 'SELECT cheques.id AS cheque_id, 
					   products.name, 
					   client_purchases.price,
					   client_purchases.price AS product_price,
					   client_purchases.amount, 
					   products.amount_label
							FROM products
								JOIN client_purchases ON client_purchases.product_id = products.id
								JOIN cheques ON cheques.id = client_purchases.cheque_id
								JOIN clients ON clients.id = cheques.client_id
							WHERE cheques.id IN ('.$placeholders.')
							ORDER BY cheque_id DESC';

		return $this->getCommon()->select($sql, $cheques);
	}
	
	public function relatePurchasesWithCheques(array $purchases, array $cheques)
	{
		$tmpCheques = [];
		foreach ($cheques as $cheque) {
			$tmpCheques[$cheque['id']] = $cheque;
		}
		
		$chequeId = null;
		foreach ($purchases as &$purchase) {
			$purchase['price'] = number_format($purchase['price'], 2);
			if ($chequeId != $purchase['cheque_id']) {
				$chequeId  = $purchase['cheque_id'];
				$chequeSum = 0;
			}
			$chequeSum += $purchase['price'] * $purchase['amount'];
			$tmpCheques[$purchase['cheque_id']]['sum'] = number_format($chequeSum, 2);
			$tmpCheques[$purchase['cheque_id']]['purchases'][] = $purchase;
		}

		return $tmpCheques;
	}
	
	public function relatePurchasesWithChequesByYear(array $purchases, array $cheques)
	{
		$tmpCheques = [];
		foreach ($cheques as $cheque) {
			$tmpCheques[$cheque['purchases_datetime']] = $cheque;
		}

		foreach ($purchases as &$purchase) {
			$purchase['price'] = number_format($purchase['price'], 2);
			$sum = $tmpCheques[$purchase['purchases_datetime']]['cheque_sum'];
			$tmpCheques[$purchase['purchases_datetime']]['sum'] = number_format($sum, 2);
			$tmpCheques[$purchase['purchases_datetime']]['purchases'][] = $purchase;
		}

		return $tmpCheques;
	}
	
	public function relatePurchasesWithChequesByMonth(array $purchases, array $cheques)
	{
		$tmpCheques = [];
		foreach ($cheques as $cheque) {
			$tmpCheques[$cheque['purchases_datetime']] = $cheque;
		}

		foreach ($purchases as &$purchase) {
			$purchase['price'] = number_format($purchase['price'], 2);
			$sum = $tmpCheques[$purchase['purchases_datetime']]['cheque_sum'];
			$tmpCheques[$purchase['purchases_datetime']]['sum'] = number_format($sum, 2);
			$tmpCheques[$purchase['purchases_datetime']]['purchases'][] = $purchase;
		}

		return $tmpCheques;
	}
}