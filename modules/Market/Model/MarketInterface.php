<?php
namespace Market\Model;

interface MarketInterface
{
	public function getClients(array $id) : array;
	public function getClient($cardNumber);
	public function getCheques($clientId, $offset, $limit);
	public function getChequesCount($clientId);
	public function getChequePurchases(array $cheques);
	public function relatePurchasesWithCheques(array $purchases, array $cheques);
}