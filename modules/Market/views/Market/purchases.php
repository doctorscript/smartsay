<script src="<?php echo $this->url()->base()?>media/js/jquery-ui-1.11.4.min.js"></script>
<div class="col-lg-12">
    <div class="green_header"><?php echo $this->translate('purchases_in_'.$this->market);?></div>
</div>
<?php
$pagination = $this->render('frontend/pagination', $pages);
?>
<div class="row">
	
	<div style="float:left;margin-top: 21px;">
	<form action="" method="get" id="purchases_form">
		<input type="hidden" name="client_id" value="<?php echo $clientId;?>" />
		<select id="cheques_order" name="order"><option value="" readonly="readonly"><?php echo $this->translate('order_purchases');?></option>';
			<?php
				$orderType = $this->escapeGet('order');
				foreach ($orders as $order) {
					$selected = $orderType == $order ? 'selected="selected"' : '';
					echo '<option value="'.$order.'" '.$selected.'>'.$this->translate('order_'.$order).'</option>';
				}
			?>
		</select>
		<select id="cheques_group" name="group"><option value="" readonly="readonly"><?php echo $this->translate('group_purchases');?></option>';
			<?php
				$groupType = $this->escapeGet('group');
				foreach ($groups as $group) {
					$selected = $groupType == $group ? 'selected="selected"' : '';
					echo '<option value="'.$group.'" '.$selected.'>'.$this->translate('group_'.$group).'</option>';
				}
			?>
		</select>
		<a style="color:#6C9B46;font-size:16px;display:inline-block;margin-left:10px;margin-top:3px;margin-bottom:3px;" href="<?php echo $this->url()->toRoute('gifts_list', [
	'lang'   => $this->translate()->getLocale(),
	'market' => $this->market
	]);?>"><img src="<?php echo $this->url()->base();?>media/img/green_gift.png" /><?php echo $this->translate('gifts');?></a>
	</form>
	</div>
	<div style="float:right;text-align:center;height: 70px;margin-right: 17px;">
	<?php echo $pagination;?>
	</div>
</div>
<script>
$(document).ready(function(){
	$('#cheques_order, #cheques_group').on('change', function(){
		$.each($('#purchases_form :input'), function(){
			var _this = $(this);
			if (_this.val() == '') {
				_this.removeAttr('name');
				console.log('name');
			}
		});
		$('#purchases_form').submit();
	});
});
</script>

<div class="row">
	<div class="col-lg-12">
		<!--Accordion start-->
	<div id="accordion">
		
		<!--Single accordion item start-->
		<?php
			foreach ($cheques as $cheque) {
				if (!preg_match('#^\d{4}-\d{2}-\d{2}\h\d{2}:\d{2}:\d{2}$#', $cheque['purchases_datetime'])) {
					$datetime = $cheque['purchases_datetime'];
				} else {
					$datetime = date('d.m.Y H:i', strtotime($cheque['purchases_datetime']));
				}
				echo '<div class="row">
						<div class="col-lg-2 col-md-3 col-sm-4 col-xs-6" style="padding-left: 0;"><span>'.$datetime.'</span></div>
						<div class="col-lg-2 col-lg-offset-8 col-md-3 col-md-offset-6 col-sm-4 col-sm-offset-4 col-xs-6" style="text-align: right; padding-right: 7px;"><span>'.htmlspecialchars($cheque['sum']).' azn</span>&nbsp;<span> '.(int)$cheque['bonus'].'</span></div>
					</div>';
				echo '<div><table class="table table-condensed" class="goods_tbl">';
				echo '<tr>
							<td style="font-weight:bold;">'.$this->translate('product').'</td>
							<td style="font-weight:bold;">'.$this->translate('count').'</td>
							<td style="font-weight:bold;">'.$this->translate('oount_numeral').'</td>
							<td style="font-weight:bold;">'.$this->translate('price').'</td>
							<td style="font-weight:bold;">'.$this->translate('total_price').'</td>
						</tr>';
				foreach ($cheque['purchases'] as $purchase) {
					if (!$groupType) {
						$price = number_format(($purchase['price'] * $purchase['amount']), 2);
					} else {
						$price = $purchase['price'];
					}
					echo '<tr>
							<td>'.htmlspecialchars($purchase['name']).'</td>
							<td>'.htmlspecialchars($purchase['amount']).'</td>
							<td>'.htmlspecialchars($purchase['amount_label']).'</td>
							<td>'.htmlspecialchars($purchase['product_price']).'</td>
							<td>'.$price.'</td>
						</tr>';
				}
				echo '</table>
				<div class="row green_bg">
					<div class="col-lg-2 col-md-3 col-sm-4 col-xs-7">
						<b>'.$this->translate('total_sum').'</b>
					</div>
					<div class="col-lg-2 col-lg-offset-8 col-md-3 col-md-offset-6 col-sm-4 col-sm-offset-4 col-xs-5">
						<b>'.htmlspecialchars($cheque['sum']).' azn</b>
					</div>
				</div>';
				echo '<div class="row green_bg" style="margin-top: 10px;">
							<div class="col-lg-2 col-md-3 col-sm-4 col-xs-7">
								<b>'.$this->translate('received_bonus').'</b>
							</div>
							<div class="col-lg-2 col-lg-offset-8 col-md-3 col-md-offset-6 col-sm-4 col-sm-offset-4 col-xs-5">
								<b>'.(int)$cheque['bonus'].'</b>
							</div>
					  </div></div>';
			}
		?>
	</div>
	<script>
	$(document).ready(function(){
		$("#accordion" ).accordion({
			heightStyle: "content",
			collapsible: true,
			beforeActivate: function(event, ui) {
				 // The accordion believes a panel is being opened
				if (ui.newHeader[0]) {
					var currHeader  = ui.newHeader;
					var currContent = currHeader.next('.ui-accordion-content');
				 // The accordion believes a panel is being closed
				} else {
					var currHeader  = ui.oldHeader;
					var currContent = currHeader.next('.ui-accordion-content');
				}
				 // Since we've changed the default behavior, this detects the actual status
				var isPanelSelected = currHeader.attr('aria-selected') == 'true';

				 // Toggle the panel's header
				currHeader.toggleClass('ui-corner-all',isPanelSelected).toggleClass('accordion-header-active ui-state-active ui-corner-top',!isPanelSelected).attr('aria-selected',((!isPanelSelected).toString()));

				// Toggle the panel's icon
				currHeader.children('.ui-icon').toggleClass('ui-icon-triangle-1-e',isPanelSelected).toggleClass('ui-icon-triangle-1-s',!isPanelSelected);

				 // Toggle the panel's content
				currContent.toggleClass('accordion-content-active',!isPanelSelected)    
				if (isPanelSelected) { currContent.slideUp(); }  else { currContent.slideDown(); }

				return false; // Cancels the default action
			}
		});
	});
	</script>
	<!--Accordion   end-->
	</div>
</div>
<div class="row">
	<div class="col-lg-12" style="text-align: right;">
		<?php echo $pagination;?>
	</div>
</div>