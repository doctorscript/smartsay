<?php
return [
	//auth errors
	'empty_card_number' 			 => 'Kartın nömrəsini daxil edin',
	'empty_phone_number' 			 => 'Mobil telefonun nömrəsinini daxil edin',
	'empty_card_barcode' 			 => 'Kartın barkodunu daxil edin',
	'invalid_card_number_or_barcode' => 'Kartın nömrəsi və ya barkodu düzgün daxil olunmayıb',
	
	//internal error
	'try_again_later' => 'Zəhmət olmasa bir az sonra yenidən cəhd edin',
	
	//market client auth
	'client_name_pattern' => 'Daxil olunan nomrəyə uyğun {name} adında müştəri tapıldi.',
	'client_not_found'	  => 'Məlumat tapılmayıb',
	'see_purchases'	  	  => 'Alış-verişləri izləmək',
	
	//purchases
	'order_much'    => 'Çoxlu xərcli olanlar',
	'order_few'     => 'Az xərcli olanlar',
	'order_near'    => 'Yaxin zamanda',
	'order_distant' => 'Keçmiş zamanda',
	'group_year'    => 'İl üzrə',
	'group_month'   => 'İl və ay üzrə',
	'purchases_in_megastore' => 'MegaStore marketində olan alış-verişlər',
	'gifts_in_megastore' 	 => 'MegaStore marketində olan hədiyyələr',
	'purchases_in_bazarstore' => 'BazarStore marketində olan alış-verişlər',
	'gifts_in_bazarstore' 	 => 'BazarStore marketində olan endirimli məhsullar',
	
	//markets
	'market_megastore' => 'MegaStore',
	'market_bazarstore' => 'BazarStore',
	'card_number' => 'Kartın nömrəsi',
	'choose_market' => 'Marketinizi seçin',
	
	//purchases
	'gifts' => 'Endirimli məhsullar',
	'product' => 'Məhsul',
	'count' => 'Say',
	'oount_numeral' => 'Kq/Ədəd',
	'price' => 'Qiymət',
	'total_price' => 'Ümumi qiymət',
	'total_sum' => 'Ümumi məbləğ',
	'received_bonus' => 'Qazanılan bonus',
	
	//purchases orders
	'order_purchases' => 'Alış-verişlərin siralanması',
	'group_purchases' => 'Alış-verişlərin qruplaşdırılması',

];