<?php
return [
	//auth errors
	'empty_card_number' 			 => 'Введите номер карты',
	'empty_phone_number' 			 => 'Введите номер телефона',
	'empty_card_barcode' 			 => 'Введите номер карты',
	'invalid_card_number_or_barcode' => 'Неверный номер или штрихкод карты',
	
	//internal error
	'try_again_later' => 'Пожалуйста, повторите попытку позже',
	
	//market client auth
	'client_name_pattern' => 'По вашему запросу найден {name}.',
	'client_not_found'	  => 'Информация не найдена',
	'see_purchases'	  	  => 'Ознакомиться с покупками',
	
	//purchases
	'order_much'    => 'Более затратные',
	'order_few'     => 'Менее затратные',
	'order_near'    => 'В ближайшем времени',
	'order_distant' => 'В прошедшем времени',
	'group_year'    => 'По годам',
	'group_month'   => 'По годам и месяцам',
	'purchases_in_megastore' => 'Покупки в маркете MegaStore',
	'gifts_in_megastore' 	 => 'Подарки в маркете MegaStore',
	'purchases_in_bazarstore' => 'Покупки в маркете BazarStore',
	'gifts_in_bazarstore' 	 => 'Продукты со скидкой в маркете BazarStore',
	
	//markets
	'market_megastore' => 'MegaStore',
	'market_bazarstore' => 'BazarStore',
	'card_number' => 'Номер карты',
	'choose_market' => 'Выберите маркет',
	
	//purchases
	'gifts' => 'Товары со скидкой',
	'product' => 'Продукт',
	'count' => 'Количество',
	'oount_numeral' => 'Кг/Штук',
	'price' => 'Цена',
	'total_price' => 'Общая стоимость',
	'total_sum' => 'Общая сумма',
	'received_bonus' => 'Заработанные бонусы',

	'order_purchases' => 'Сортировка покупок',
	'group_purchases' => 'Группировка покупок',
];