<?php
namespace Market;

use Core\Mvc\MvcEvent;
use DoctorScript\EventManager\EventInterface;

class Module
{
	public function getConfig()
	{
		return require_once(__DIR__ . '/config/module.config.php');
	}
}