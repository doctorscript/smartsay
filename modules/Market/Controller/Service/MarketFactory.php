<?php
namespace Market\Controller\Service;

use DoctorScript\ServiceManager\ServiceFactoryInterface;
use DoctorScript\ServiceManager\ServiceLocatorInterface;
use Market\Controller\MarketController;

class MarketFactory implements ServiceFactoryInterface
{
	public function createService(ServiceLocatorInterface $serviceLocator)
	{
		$parentLocator = $serviceLocator->getServiceLocator();
		$session 	   = $parentLocator->get('DoctorScript\Session\Storage');
		$market 	   = $parentLocator->get('Market\Model\MarketInterface');
		
		$action = $parentLocator->get('DoctorScript\Mvc\Service\MvcEventFactory')
								->getRouteMatch()
								->getParam('action');
		
		$controller = new MarketController($market, $session);
		if (in_array($action, ['clientAuth', 'purchases'])) {
			$controller->setSystemUser($parentLocator->get('SystemUser\Model\SystemUser'));
		}
		
		if (in_array($action, ['clientSearch' ,'clientAuth'])) {
			$controller->setClientCardValidator($parentLocator->get('Market\Validator\ClientCard\ValidatorInterface'));
		}

		return $controller;
	}
}