<?php
namespace Gifts\Model\Market\Adapter;

use Common\Model\AbstractModel;
use Gifts\Model\GiftsInterface;

class MegaStore extends AbstractModel implements GiftsInterface
{
	public function getList()
	{
		return $this->getCommon()->select('SELECT name, price, image FROM gifts');
	}
}