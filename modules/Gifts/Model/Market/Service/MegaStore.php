<?php
namespace Gifts\Model\Market\Service;

use DoctorScript\ServiceManager\ServiceFactoryInterface;
use DoctorScript\ServiceManager\ServiceLocatorInterface;
use Gifts\Model\Market\Adapter\MegaStore as MegaStoreAdapter;

class MegaStore implements ServiceFactoryInterface
{
	public function createService(ServiceLocatorInterface $serviceLocator)
	{
		$common  = $serviceLocator->get('Common\Model\CommonInterfaceMegaStore');
		$adapter = new MegaStoreAdapter($common);

		return $adapter;
	}
}