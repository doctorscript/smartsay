<?php
namespace Gifts\Controller\Service;

use DoctorScript\ServiceManager\ServiceFactoryInterface;
use DoctorScript\ServiceManager\ServiceLocatorInterface;
use Gifts\Controller\GiftsController;

class GiftsFactory implements ServiceFactoryInterface
{
	public function createService(ServiceLocatorInterface $serviceLocator) : GiftsController
	{
		$parentLocator = $serviceLocator->getServiceLocator();
		$market		   = $parentLocator->get('DoctorScript\Router\RouterInterface')->getRouteMatch()->getParam('market');
		$market = 'megastore';
		$adapter	   = $this->param2Adapter($market, $parentLocator);

		$gifts 	   	   = $parentLocator->get('Gifts\Model\Market\Adapter\\'.$adapter);

		$controller = new GiftsController($gifts);
		return $controller;
	}
	
	public function param2Adapter(string $param, ServiceLocatorInterface $serviceLocator) : string
	{
		return 'MegaStore';
		$config = $serviceLocator->get('config');
		if (!isset($config['markets']) || !is_array($config['markets'])) {
			throw new \RuntimeException('Markets config is not defined');
		}
		$param = 'megastore';

		if (!isset($config['markets'][$param])) {
			throw new \RuntimeException(sprintf(
				'Cannot get market gifts adapter for %s, market is not exists', $param
			));
		}
		
		return $config['markets'][$param];
	}
}