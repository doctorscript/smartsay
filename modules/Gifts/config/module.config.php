<?php
return [
	'routes' => [
		'gifts_list' => [
			'pattern' 	 => '(?<lang>az|ru)/market/(?<market>[\w]+)/gifts/?',
			'controller' => 'Gifts\Controller\GiftsController',
			'action' 	 => 'list',
		],
	],
	'controller_manager' => [
		'Gifts\Controller\GiftsController' => 'Gifts\Controller\Service\GiftsFactory',
	],
	'service_manager' => [
		'Gifts\Model\Market\Adapter\MegaStore' => 'Gifts\Model\Market\Service\MegaStore',
	],
	'translator' => [
		'paths' => [
			__DIR__ . '/../translates'
		],
	]
];