<?php
return [
	'routes' => [
		'admin_system_users' => [
			'pattern' 	 => 'x/_adminzone/system_users/list/?',
			'controller' => 'AdminSystemUser\Controller\AdminSystemUserController',
			'action' 	 => 'list',
		],
	],
	'controller_manager' => [
		'AdminSystemUser\Controller\AdminSystemUserController' => 'AdminSystemUser\Controller\Service\AdminSystemUserFactory',
	],
	'service_manager' => [
		'AdminSystemUser\Model\AdminSystemUserInterface' => 'AdminSystemUser\Model\Service\AdminSystemUserFactory',
	],
	'translator' => [
		'paths' => [
			__DIR__ . '/../translates'
		],
	]
];