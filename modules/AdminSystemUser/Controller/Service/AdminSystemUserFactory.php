<?php
namespace AdminSystemUser\Controller\Service;

use DoctorScript\ServiceManager\ServiceFactoryInterface;
use DoctorScript\ServiceManager\ServiceLocatorInterface;
use AdminSystemUser\Controller\AdminSystemUserController;

class AdminSystemUserFactory implements ServiceFactoryInterface
{
	public function createService(ServiceLocatorInterface $serviceLocator)
	{
		$parentLocator = $serviceLocator->getServiceLocator();
		
		$adminSystemUser = $parentLocator->get('AdminSystemUser\Model\AdminSystemUserInterface');
		$session   = $parentLocator->get('DoctorScript\Session\Storage');

		$controller = new AdminSystemUserController($adminSystemUser, $session);
		return $controller;
	}
}