<?php
namespace AdminSystemUser\Model\Service;

use DoctorScript\ServiceManager\ServiceFactoryInterface;
use DoctorScript\ServiceManager\ServiceLocatorInterface;
use AdminSystemUser\Model\AdminSystemUser;

class AdminSystemUserFactory implements ServiceFactoryInterface
{
	public function createService(ServiceLocatorInterface $serviceLocator)
	{
		$common    = $serviceLocator->get('Common\Model\CommonInterface');
		$AdminSystemUser = new AdminSystemUser($common);
		
		return $AdminSystemUser;
	}
}