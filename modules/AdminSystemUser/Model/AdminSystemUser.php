<?php
namespace AdminSystemUser\Model;

use Common\Model\AbstractModel;

class AdminSystemUser extends AbstractModel implements AdminSystemUserInterface
{	
	private $systemUsersTable = 'system_users';
	
	public function getList($offset, $limit, $search = null)
	{
		$params = [
			':offset' => $offset,
			':limit'  => $limit,
		];
		
		$where = '';
		if ($search) {
			$where .= 'CONCAT_WS(" ", name, surname) LIKE :search_0 OR
					   CONCAT_WS(" ", surname, name) LIKE :search_1 OR
					   email LIKE :search_2';
			for ($i = 0; $i < 3; $i++) {
				$params[':search_'.$i] = '%'.$search.'%';
			}
		}
		if ($where) {
			$where = ' WHERE '.$where;
		}
		
		$sql = 'SELECT name, surname, email, `reg_datetime`
					   FROM '.$this->systemUsersTable.'
							'.$where.'
					   ORDER BY id DESC
					   LIMIT :offset, :limit';

		return $this->getCommon()->select($sql, $params);
	}
	
	public function getCount($search = null)
	{
		$params = [];
		$where  = '';
		if ($search) {
			$where .= 'CONCAT_WS(" ", name, surname) LIKE :search_0 OR
					   CONCAT_WS(" ", surname, name) LIKE :search_1 OR
					   email LIKE :search_2';
			for ($i = 0; $i < 3; $i++) {
				$params[':search_'.$i] = '%'.$search.'%';
			}
		}
		if ($where) {
			$where = ' WHERE '.$where;
		}

		$sql = 'SELECT COUNT(id) AS cnt FROM '.$this->systemUsersTable.$where;
		$result = $this->getCommon()->selectOne($sql, $params);
		return $result ? $result['cnt'] : 0;
	}
}