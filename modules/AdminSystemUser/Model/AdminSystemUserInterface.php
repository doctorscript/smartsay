<?php
namespace AdminSystemUser\Model;

interface AdminSystemUserInterface
{
	public function getList($offset, $limit, $search);
	public function getCount();
}