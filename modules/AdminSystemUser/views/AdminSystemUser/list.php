    <div class="content">
        <div class="header">
            <h1 class="page-title">Список пользователей системы</h1>
	<div class="form-group input-group" style="position:absolute;width:290px;right: 5px;top: 13px;">
		<form action="">
		<input type="text" name="search" class="form-control" style="width:250px;" value="<?php echo $this->escapeGet('search')?>">
		<span class="input-group-btn"><button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button></span>
	</div>
        </div>
<div class="main-content">
	<table class="table">
	  <thead>
		<tr>
		  <th>Имя</th>
		  <th>Фамилия</th>
		  <th>E-mail</th>
		  <th>Дата регистрации</th>
		</tr>
	  </thead>
	  <tbody>
		<?php
			foreach ($systemUsers as $systemUser) {
				echo '<tr>
						  <td>'.htmlspecialchars($systemUser['name']).'</td>
						  <td>'.htmlspecialchars($systemUser['surname']).'</td>
						  <td>'.htmlspecialchars($systemUser['email']).'</td>
						  <td>'.htmlspecialchars($systemUser['reg_datetime']).'</td>
					 </tr>';
			}
		?>
	  </tbody>
	</table>
<?php
if (!empty($pages['pages']) && count($pages['pages']) > 1) {
?>
	<ul class="pagination" style="float:right;">
	  <li><a href="#">&laquo;</a></li>
	  <?php
		$baseLink = $this->url()->toRoute('admin_system_users') . '/';
		$params   = ['search' => $this->escapeGet('search')];
		foreach ($pages['pages'] as $page) {
			$params['page'] = $page;
			$params = array_filter($params);
			$pagination = $baseLink .'?'. http_build_query($params);
			echo '<li><a href="'.$pagination.'">'.$page.'</a></li>';
		}
	  ?>
	  <li><a href="#">&raquo;</a></li>
	</ul>
<?php
}
?>
</div>
</div>