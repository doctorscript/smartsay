<?php
return [
	//system user registration errors
	//'too_short_user_name'     => '"Adınız" xanası 2 simvoldan az olmalı deyil',
	//'too_short_user_surname'  => '"Soyadınız" xanası 2 simvoldan az olmalı deyil',
	'incorrect_user_email'    => 'Введите корректный E-mail адрес',
	'user_already_exists'	  => 'Пользователь %s уже зарегистрирован',
	'too_short_user_password' => 'Минимальная длина пароля 6 символов',
	'passwords_not_equal' 	  => 'Пароли не совпадают',
	
	//password recover
	'incorrect_email_on_recover' => 'Введите корректный E-mail адрес',
	'password_recover_success'   => 'Новый пароль отправлен на указанный E-mail адрес',
	
	//edit user data
	'invalid_b_day'   => 'Укажите корректный день рождения',
	'invalid_b_month' => 'Укажите корректный месяц рождения',
	'invalid_b_year'  => 'Укажите корректный год рождения',
	'invalid_gender'  => 'Укажите корректный пол',
	'invalid_mobile_phone_prefix'  => 'Укажите корректный код мобильного оператора',
	'invalid_mobile_phone_format'  => 'Укажите корректный номер телефона',
	'invalid_current_pwd'  => 'Текущий пароль введен неверно',
	'system_user_edit_success'  => 'Информация успешно обновлена',
	
	//password recovering
	'user_password_recover_message' => "Здравствуйте %s.\nВаш новый пароль: %s\nВы можете изменить данный пароль в меню \"Личная информация\"\nС уважением, SmartSay.az",
	'user_password_recover_subject' => 'Восстановление пароля',
	
	//months
	'1_month' => 'Январь',
	'2_month' => 'Февраль',
	'3_month' => 'Март',
	'4_month' => 'Апрель',
	'5_month' => 'Май',
	'6_month' => 'Июнь',
	'7_month' => 'Июль',
	'8_month' => 'Август',
	'9_month' => 'Сентябрь',
	'10_month' => 'Октябрь',
	'11_month' => 'Ноябрь',
	'12_month' => 'Декабрь',
	
	//auth
	'password_recovery' => 'Восстановление пароля',
	'password_recovery_email' => 'На указанный E-mail адрес будут отправлены инструкции для восстановления пародя',
	'send'	=> 'Отправить',
	'password' => 'Пароль',
	'remember_me' => 'Запомнить меня',
	'forget_password' => 'Забыли пароль?',
	'be_registered' => 'Регистрация',
	'coming_soon' => 'Скоро',
	'login' => 'ВОЙТИ',
	'enter_email' => 'Введите E-mail адрес',
	
	//register && auth
	'register_in_system' => 'Регистрация в системе',
	'name' => 'Имя',
	'surname' => 'Фамилия',
	'personal_information' => 'Персональная информация',
	'password_again' => 'Повторите пароль',
	'code' => 'Код',
	'birthdate' => 'Дата рожд&shy;ения',
	'day' => 'День',
	'month' => 'Месяц',
	'year' => 'Год',
	'gender' => 'Пол',
	'male' => 'Мужской',
	'female' => 'Женский',
	'mobile_phone' => 'Мобильный номер',
	'new_password' => 'Новый пароль',
	'new_password_repeat' => 'Новый пароль ещё раз',
	'current_password' => 'Текущий пароль',
	'save' => 'Сохранить',
	
	//project description
	'what_is_smartsay' => 'С помощью программы SmartSAY вы можете быть в курсе о ваших покупках, знать какие продукты вы покупаете чаще всего а на какие тратите больше денег.',
	'bonus_info' => 'Знать количество своих бонусов и иметь информацию о том, какие подарки вы можете приобрести за эти бонусы а так же знать об акциях, проводимых маркетами.',
	'spending_info' => 'Просматривать свои покупки за определенный период, сортировать их по сумме, а так же узнавать, сколько бонусов вы заработали сделав определенную покупку.',
	'spending_follow_info' => 'Контролировать ваши расходы и поддерживать баланс собственного бюджета. Анализировать прочие расходы.',
	'market_discounts_info' => 'Постоянно быть в курсе о акциях и скидках на товары в маркетах.',
];