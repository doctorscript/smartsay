<?php
return [
	//system user registration errors
	'too_short_user_name'     => '"Adınız" xanası 2 simvoldan az olmalı deyil',
	'too_short_user_surname'  => '"Soyadınız" xanası 2 simvoldan az olmalı deyil',
	'incorrect_user_email'    => 'E-mail düzgün daxil olunmayıb',
	'user_already_exists'	  => 'İstifadəçi %s artıq qeydiyyatdan keçib',
	'too_short_user_password' => 'Şifrə 6 simvoldan az olmalı deyil',
	'passwords_not_equal' 	  => 'Şifrələr uyğun deyil',
	
	//password recover
	'incorrect_email_on_recover' => 'E-mail düzgün daxil olunmayıb',
	'password_recover_success'   => 'Yeni şifrə göstərilən ünvana göndərildi.',
	
	//edit user data
	'invalid_b_day'   => 'Doğum tarixinin günü düzgün seçilməyib',
	'invalid_b_month' => 'Doğum tarixinin ayı düzgün seçilməyib',
	'invalid_b_year'  => 'Doğum tarixinin ili düzgün seçilməyib',
	'invalid_gender'  => 'Cins düzgün seçilməyib',
	'invalid_mobile_phone_prefix'  => 'Mobil telefonu operatoru düzgün seçilməyib',
	'invalid_mobile_phone_format'  => 'Mobil telefonu düzgün daxil olunmayıb',
	'invalid_current_pwd'  => 'Cari şifrə düzgün daxil olunmayıb',
	'system_user_edit_success'  => 'Məlumat uğurla yeniləndi',
	
	//password recovering
	'user_password_recover_message' => "Salam %s.\nSizin yeni şifrəniz: %s\nGöstərilən şifrəni \"Şəxsi məlumatlar\" səhifəsində dəyişə bilərsiniz\nHörmətlə, SmartSay.az",
	'user_password_recover_subject' => 'Şifrənin bərpası',
	
	//months
	'1_month' => 'Yanvar',
	'2_month' => 'Fevral',
	'3_month' => 'Mart',
	'4_month' => 'Aprel',
	'5_month' => 'May',
	'6_month' => 'İyun',
	'7_month' => 'İyul',
	'8_month' => 'Avqust',
	'9_month' => 'Sentyabr',
	'10_month' => 'Oktyabr',
	'11_month' => 'Noyabr',
	'12_month' => 'Dekabr',
	
	//auth
	'password_recovery' => 'Şifrənin bərpa edilməsi',
	'password_recovery_email' => 'Göstərdiyiniz E-mail-ə şifrənin bərpası üçün məktub göndəriləcək',
	'send'	=> 'Göndər',
	'password' => 'Şifrə',
	'remember_me' => 'Sistemdən çıxma',
	'forget_password' => 'Şifrəni unutdum',
	'be_registered' => 'Qeydiyyatdan keç',
	'coming_soon' => 'Tezliklə',
	'login' => 'DAXİL OL',
	'enter_email' => 'E-mail ünvanınızı daxil edin',
	
	//register && auth && edit
	'register_in_system' => 'Sistemdə qeydiyyat',
	'name' => 'Ad',
	'surname' => 'Soyad',
	'personal_information' => 'Şəxsi məlumatlar',
	'password_again' => 'Şifrə təkrar',
	'code' => 'Kod',
	'birthdate' => 'Dogum tarixi',
	'day' => 'Gün',
	'month' => 'Ay',
	'year' => 'İl',
	'gender' => 'Cins',
	'male' => 'Kişi',
	'female' => 'Qadın',
	'mobile_phone' => 'Mobil nömrə',
	'new_password' => 'Yeni şifrə',
	'new_password_repeat' => 'Yeni şifrə təkrar',
	'current_password' => 'Cari şifrə',
	'save' => 'Yadda saxla',
	
	//project description
	'what_is_smartsay' => 'SmartSAY proqramının vasitəsi ilə daima alış-verişləriniz haqqında məlumatlar əldə edə bilərsiniz. 
						Hansi məhsulları daha çox alırsınız və hansılarına daha cox xərc edirsiniz',
	'bonus_info' => 'Bonus hesabınızdan daima məlumat almaq imkanı. Bu bonuslarla hansı hədiyyələr və ya hansı endirimlər olduğu haqqında məlumat.',
	'spending_info' => 'Xərclərin bütün ayrıntılari ilə gorə bilmə imkanı.(Tarix, saat, odəniş məbləği, bonus və s.) Grafik halda xərclərin ayrı-ayrılıqda təsviri.',
	'spending_follow_info' => 'Xərclərə nəzarət, budcəni daima balansda saxlamaq imkanı. Digər xərclərinizi də bir arada gormək imkani.',
	'market_discounts_info' => 'Marketinizdə daima davam edən Endirim aksiyaları və məhsullara olunan endirimlərdən xəbərdar olacaqsınız',
];