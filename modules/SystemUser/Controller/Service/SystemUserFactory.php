<?php
namespace SystemUser\Controller\Service;

use DoctorScript\ServiceManager\ServiceFactoryInterface;
use DoctorScript\ServiceManager\ServiceLocatorInterface;
use SystemUser\Controller\SystemUserController;
use SystemUser\Model\SystemUser;
use Common\Model\MustCaptchaChecker;

class SystemUserFactory implements ServiceFactoryInterface
{
	public function createService(ServiceLocatorInterface $serviceLocator)
	{
		$parentLocator = $serviceLocator->getServiceLocator();

		$session 	= $parentLocator->get('DoctorScript\Session\Storage');
		$captcha	= $parentLocator->get('Common\Service\CaptchaFactory');
		$controller = new SystemUserController($session, $captcha);
		
		$action = $parentLocator->get('DoctorScript\Mvc\Service\MvcEventFactory')
								->getRouteMatch()
								->getParam('action');
		
		if (in_array($action, ['auth', 'recover'])) {
			$controller->setBlackList($parentLocator->get('Common\Model\BlackListInterface'));
		}
		
		if ($action != 'captcha') {
			$controller->setSystemUser($parentLocator->get('SystemUser\Model\SystemUser'));
		}
		
		return $controller;
	}
}