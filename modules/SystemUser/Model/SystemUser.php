<?php
namespace SystemUser\Model;

use Common\Model\AbstractModel;

class SystemUser extends AbstractModel implements SystemUserInterface
{	
	private $systemUserTable  = 'system_users';
	private $systemUserClientsTable = 'system_user_clients';
	
	public function register(array $user)
	{
		$common = $this->getCommon();
		$common->insert($this->systemUserTable, $user);
		return $common->lastInsertId();
	}
	
	public function userExists($email)
	{
		return $this->getCommon()->exists($this->systemUserTable, 'email', $email);
	}
	
	public function auth($email)
	{
		$common = $this->getCommon();
		$data = $common->selectOne('SELECT `id`, `password` 
										FROM `'.$this->systemUserTable.'` 
									WHERE `email`=:email', [':email' => $email]);
		return $data;
	}

	public function updatePassword($email, $newPassword)
	{
		$data = [
			':email' 	=> $email,
			':password' => $newPassword
		];
	
		$common = $this->getCommon();
		return $common->query('UPDATE `'.$this->systemUserTable.'` SET `password`=:password WHERE `email`=:email', $data);
	}
	
	public function update(array $user)
	{
		$pwdSql    = isset($user[':password']) ? ', `password`=:password' : '';
		$bDateSql  = isset($user[':birthdate']) ? ', `birthdate`=:birthdate' : '';
		$genderSql = isset($user[':gender']) ? ', `gender`=:gender' : '';
		$phoneSql  = '';
		if (isset($user[':mobile_phone_prefix']) && isset($user[':mobile_phone'])) {
			$phoneSql  = ', `mobile_phone_prefix`=:mobile_phone_prefix';
			$phoneSql .= ', `mobile_phone`=:mobile_phone';
		}
		
		$sql = 'UPDATE `'.$this->systemUserTable.'` SET 
				`name`=:name, `surname`=:surname'.$pwdSql.$bDateSql.$genderSql.$phoneSql.' WHERE `id`=:id';
	/* echo '<pre>';
	print_r($user);
		echo $sql;die; */
		return $this->getCommon()->query($sql, $user);
	}
	
	public function addClient($clientId, $userId)
	{
		try {
			$result = $this->getCommon()->insert($this->systemUserClientsTable, [
				'client_id' 	 => $clientId,
				'system_user_id' => $userId,
				'target_database' => 'bazarstore'
			]);
		} catch (\PDOException $e) {
			$result = false;
		}
		return $result;
	}
	
	public function hasClient($systemUserId, $clientId, $targetDatabase)
	{
		$sql = 'SELECT system_user_clients.client_id
					FROM system_user_clients
						JOIN system_users ON system_users.id = system_user_clients.system_user_id
					WHERE system_users.id=:system_user_id
					AND system_user_clients.client_id=:client_id
					AND system_user_clients.target_database=:target_database';
		return $this->getCommon()->selectOne($sql, [
			':system_user_id'  => $systemUserId,
			':client_id' 	   => $clientId,
			':target_database' => $targetDatabase,
		]);
	}
	
	public function remember($userId, $rememberHash, $isMobile)
	{	
		$data = [':user_id' => $userId];

		$rememberSql = '';
		
		if ($isMobile) {
			$rememberSql = 'mobile_remember_hash=:mobile_remember_hash';
			$data[':mobile_remember_hash'] = $rememberHash;
		} else {
			$rememberSql  = 'remember_hash=:remember_hash';
			$rememberExp  = strtotime('+1 day', time());
			setcookie('remember_me', $rememberHash, $rememberExp);
			$data[':remember_hash'] = $rememberHash;
		}
		
		$sql = 'UPDATE '.$this->systemUserTable.' SET '.$rememberSql.', remember_datetime=NOW() WHERE id=:user_id';
		
		$this->getCommon()->query($sql, $data);
	}
	
	public function forget()
	{
		$sql = 'UPDATE '.$this->systemUserTable.' SET remember_hash=NULL, remember_datetime=NULL
				WHERE remember_datetime < (NOW() - INTERVAL 1 DAY)';
		$this->getCommon()->query($sql);
	}
	
	public function getRememberedId($cookieHash)
	{
		if (!is_string($cookieHash)) {
			return false;
		}
		$userId = (int)strstr($cookieHash, '_hashed', true);

		$sql = 'SELECT id 
					FROM '.$this->systemUserTable.' 
				WHERE id=:user_id 
				AND remember_hash=:remember_hash
				AND remember_datetime > (NOW() - INTERVAL 1 DAY)';
		$result = $this->getCommon()->selectOne($sql, [
			':user_id'   	 => $userId,
			':remember_hash' => $cookieHash,
		]);

		if (!$result) {
			$sql = 'SELECT id 
						FROM '.$this->systemUserTable.' 
					WHERE id=:user_id 
					AND mobile_remember_hash=:remember_hash';
			$result = $this->getCommon()->selectOne($sql, [
				':user_id'   	 => $userId,
				':remember_hash' => $cookieHash,
			]);
			
			if (!$result) {
				return false;
			}
		}
		return $userId;
	}
	
	public function getPasswordHashById($id)
	{
		$sql = 'SELECT password AS hash FROM '.$this->systemUserTable.' WHERE id=:id';
		$pwd = $this->getCommon()->selectOne($sql, [
			':id' => $id
		]);
		return isset($pwd['hash']) ? $pwd['hash'] : false;
	}
	
	public function get(int $id)
	{
		$sql = 'SELECT name, surname, gender, 
					   birthdate, mobile_phone_prefix, mobile_phone 
							FROM '.$this->systemUserTable.'
					   WHERE id=:id';
		return $this->getCommon()->selectOne($sql, [
			':id' => $id
		]);
	}
	
	public function getClients($id)
	{
		//array_column by target_database and search clients using IN
		$dbListSql = 'SELECT client_id, target_database 
						FROM system_user_clients 
							JOIN system_users ON system_users.id = system_user_clients.system_user_id
						WHERE system_users.id=:system_user_id
						ORDER BY target_database';
		$results = $this->getCommon()->select($dbListSql, [
			':system_user_id' => $id,
		]);
		if (empty($results)) {
			return [];
		}
		$clients = $resultClients = [];
		foreach ($results as $result) {
			$clients[$result['target_database']][] = $result['client_id'];
		}
		
		$adapterAliases = ['bazarstore' => 'MegaStore'];
		foreach ($clients as $targetDatabase => $clientList) {
			$adapterName   = 'Market\Model\Adapter\\'.$adapterAliases[$targetDatabase];
			$adapter       = $this->getCommon()->getServiceLocator()->get($adapterName);
			$resultClients = array_merge($resultClients, $adapter->getClients($clientList));
		}

		return $resultClients;
		
		foreach ($results as $result) {
			$adapterName = 'Market\Model\Adapter\\'.$adapterAliases[$result['target_database']];
			$adapter     = $this->getCommon()->getServiceLocator()->get($adapterName);
			$clients     = array_merge($clients, $adapter->getClients($id));
		}
		
		return $clients;
	}
	
	public function getNameByEmail(string $email)
	{
		$sql	= 'SELECT CONCAT_WS(" ", name, surname) AS name FROM system_users WHERE email=:email';
		$result = $this->getCommon()->selectOne($sql, [
			':email' => $email
		]);
		return isset($result['name']) ? $result['name'] : false;
	}
}