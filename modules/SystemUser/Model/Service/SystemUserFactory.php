<?php
namespace SystemUser\Model\Service;

use DoctorScript\ServiceManager\ServiceFactoryInterface;
use DoctorScript\ServiceManager\ServiceLocatorInterface;
use SystemUser\Model\SystemUser;

class SystemUserFactory implements ServiceFactoryInterface
{
	public function createService(ServiceLocatorInterface $serviceLocator)
	{
		$common 	= $serviceLocator->get('Common\Model\CommonInterface');
		$SystemUser = new SystemUser($common);
		
		return $SystemUser;
	}
}