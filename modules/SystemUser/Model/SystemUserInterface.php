<?php
namespace SystemUser\Model;

interface SystemUserInterface
{
	public function register(array $user);
	public function auth($email);
	public function updatePassword($email, $newPassword);
	public function update(array $user);
	public function addClient($clientId, $userId);
	public function remember($userId, $rememberHash, $isMobile);
	public function forget();
}