<?php
return [
	'routes' => [
		'registration' => [
			'pattern' 	 => '(?<lang>az|ru)/user/registration/?',
			'controller' => 'SystemUser\Controller\SystemUserController',
			'action' 	 => 'register',
		],
		'user_authentication' => [
			'pattern' 	 => '(?<lang>az|ru)?',
			'controller' => 'SystemUser\Controller\SystemUserController',
			'action' 	 => 'auth',
		],
		'system_user_clients' => [
			'pattern' 	 => '(?<lang>az|ru)/user/clients',
			'controller' => 'SystemUser\Controller\SystemUserController',
			'action' 	 => 'clients',
		],
		'system_user_logout' => [
			'pattern' 	 => '(?<lang>az|ru)/logout',
			'controller' => 'SystemUser\Controller\SystemUserController',
			'action' 	 => 'logout',
		],
		'recover_user_password' => [
			'pattern' 	 => '(?<lang>az|ru)/user/recover/?',
			'controller' => 'SystemUser\Controller\SystemUserController',
			'action' 	 => 'recover',
		],
		'edit_user_data' => [
			'pattern' 	 => '(?<lang>az|ru)/user/edit/?',
			'controller' => 'SystemUser\Controller\SystemUserController',
			'action' 	 => 'edit',
		],
		'system_user_captcha' => [
			'pattern' 	 => '(?<lang>az|ru)/user/credentials/captcha/(?<handler>registration|auth|recover)',
			'controller' => 'SystemUser\Controller\SystemUserController',
			'action' 	 => 'captcha',
		],
	],
	'controller_manager' => [
		'SystemUser\Controller\SystemUserController' => 'SystemUser\Controller\Service\SystemUserFactory',
	],
	'service_manager' => [
		'SystemUser\Model\SystemUser' => 'SystemUser\Model\Service\SystemUserFactory',
	],
	'translator' => [
		'paths' => [
			__DIR__ . '/../translates'
		],
	]
];