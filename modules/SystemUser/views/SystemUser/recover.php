<h1>Восстановление пароля</h1>
<form action="" method="post">
	<input type="text" name="email" /> <br />
	<?php
		if ($needCheckCaptcha) {
			echo '<input type="text" name="captcha" /> <img src="'.$this->url()->toRoute('system_user_captcha', [
					'lang' 	  => $this->translate()->getLocale(),
					'handler' => 'recover'
				]).'" />';
		}
	?>
	<input type="submit" />
</form>