<div style="position: relative;">
	<div class="green_header green_header_margin_bottom"><?php echo $this->translate('personal_information');?></div>
</div>
<?php
if (!empty($errors)) {
	foreach ($errors as $error) {
		echo '<div style="color:#c00;">'.$error.'</div>';
	}
}
if ($session->has('system_user_edit_success')) {
	echo '<div style="color:#64A039;font-weight:bold;">'.$session->get('system_user_edit_success').'</div>';
	$session->delete('system_user_edit_success');
}
?>
<script>
$(document).ready(function(){
	$('input[name="gender"]:checked').parent().addClass('c_d_w_c');
});
</script>
<div class="row">
	<div class="col-lg-12">
		<form action="" method="POST" autocomplete="off">
			<!--name & surname start-->
			<div class="row no-minus-margin" style="background-color: #81C34D;">
				<div class="col-lg-3 col-md-3 col-sm-4">
					<div class="custom_inp_fluid">
						<input class="white_placeholder" type="text" name="name" value="<?php echo $this->escapePost('name', $user['name']);?>" placeholder="<?php echo $this->translate('name');?>"/>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-4 ">
					<div class="custom_inp_fluid">
						<input class="white_placeholder" type="text" name="surname" value="<?php echo $this->escapePost('surname', $user['surname']);?>" placeholder="<?php echo $this->translate('surname');?>"/>
					</div>
				</div>
			</div>
			<!--name & surname   end-->
			
			<!--birthdate & gender start-->
			<div class="row no-minus-margin" style="background-color: #81C34D;">
				<div class="col-lg-3 col-md-3 col-sm-4 " style="padding-top: 5px; padding-bottom: 5px;">
					<?php
						if (!empty($user['birthdate'])) {
							$birthdate = \DateTime::createFromFormat('Y-m-d', $user['birthdate']);
						}
					?>
					<table class="t_w_sel">
						<tr>
							<td>
								<span class="t_on_gr"><?php echo $this->translate('birthdate');?></span>
							</td>
							<td>
								<select name="b_day" class="ed_pg_sel _input_standard_height">
									<option value=""><?php echo $this->translate('day');?></option>
									<?php
										foreach ($days as $day) {
											$selected = isset($birthdate) && $birthdate->format('d') == $day ? 'selected="selected"':'';
											echo '<option value="'.$day.'" '.$selected.'>'.$day.'</option>';
										}
									?>
								</select>
							</td>
							<td>
								<select name="b_month" class="ed_pg_sel _input_standard_height" style="width: 69px;">
									<option value=""><?php echo $this->translate('month');?></option>
									<?php
										foreach ($months as $month) {
											$selected = isset($birthdate) && $birthdate->format('m') == $month ? 'selected="selected"':'';
											echo '<option value="'.$month.'" '.$selected.'>'.$this->translate($month.'_month').'</option>';
										}
									?>
								</select>
							</td>
							<td>
								<select name="b_year" class="ed_pg_sel _input_standard_height">
									<option value=""><?php echo $this->translate('year');?></option>
									<?php
										$years = array_reverse($years);
										foreach ($years as $year) {
											$selected = isset($birthdate) && $birthdate->format('Y') == $year ? 'selected="selected"':'';
											echo '<option value="'.$year.'" '.$selected.'>'.$year.'</option>';
										}
									?>
								</select>
							</td>
						</tr>
					</table>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-4 " style="padding-top: 14px; padding-bottom: 5px;">
					<span class="t_on_gr"><?php echo $this->translate('gender');?>: </span>
					<?php
						$maleChecked   = !empty($_POST['gender']) && $_POST['gender'] == 'male' ? 'checked="checked"'
										 : ($_SERVER['REQUEST_METHOD'] != 'POST' && !empty($user['gender']) && $user['gender'] == 'male' ? 'checked="checked"' : '');
						$femaleChecked = !empty($_POST['gender']) && $_POST['gender'] == 'female' ? 'checked="checked"'
										 : ($_SERVER['REQUEST_METHOD'] != 'POST' && !empty($user['gender']) && $user['gender'] == 'female' ? 'checked="checked"' : '');
					?>
					<label><input type="radio" name="gender" value="male" class="t_on_gr custom_ch" <?php echo $maleChecked;?>/><?php echo $this->translate('male');?></label>
					<label><input type="radio" name="gender" value="female" class="t_on_gr custom_ch" <?php echo $femaleChecked;?>/><?php echo $this->translate('female');?></label>
				</div>
			</div>
			<!--birthdate & gender   end-->
			
			<!--name & email start-->
			<div class="row no-minus-margin" style="background-color: #81C34D;">
				<div class="col-lg-3 col-md-3 col-sm-4 " style="margin-top: 10px;">
					<table class="t_w_sel">
						<tr>
							<td>
								<select class="ed_pg_sel _input_standard_height" name="mobile_phone_prefix">
									<?php
										foreach ($validPhonePrefixList as $validPhonePrefix) {
											$selected = $user['mobile_phone_prefix'] == $validPhonePrefix ? 'selected="selected"' : '';
											echo '<option value="'.$validPhonePrefix.'" '.$selected.'>'.$validPhonePrefix.'</option>';
										}
									?>
								</select>
							</td>
							<td>
								<div class="custom_inp_dsp_i_b">
									<input class="white_placeholder" type="text" name="mobile_phone" placeholder="<?php echo $this->translate('mobile_phone');?>" style="width: 100%; padding-left: 10px;height:35px;" class="_input_standard_height" value="<?php echo $this->escapePost('mobile_phone', $user['mobile_phone'])?>"/>
								</div>
							</td>
						</tr>
					</table>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-4 ">
					<div class="custom_inp_fluid">
						<input class="white_placeholder" type="password" name="password" value="" placeholder="<?php echo $this->translate('new_password');?>"/>
					</div>
				</div>
			</div>
			<!--name & email   end-->
			
			<!--password & repeat password start-->
			<div class="row no-minus-margin" style="background-color: #81C34D;">
				<div class="col-lg-3 col-md-3 col-sm-4 ">
					<div class="custom_inp_fluid">
						<input class="white_placeholder" type="password" name="c_password" value="" placeholder="<?php echo $this->translate('new_password_repeat');?>"/>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-4 ">
					<div class="custom_inp_fluid">
						<input class="white_placeholder" type="password" name="current_pwd" value="" placeholder="<?php echo $this->translate('current_password');?>"/>
					</div>
				</div>
			</div>
			<!--password & repeat password   end-->
			<!--submit button start-->
			<div class="row no-minus-margin" style="background-color: #81C34D; padding-bottom: 10px; padding-top: 10px; border-bottom-left-radius: 5px; border-bottom-right-radius: 5px;">
				<div class="col-lg-3 col-md-3 col-sm-4">
					<input type="submit" name="submit" value="<?php echo $this->translate('save');?>" class="button button_grey button_block"/>
				</div>
			</div>
			<!--submit button   end-->
			<input type="hidden" name="csrf" value="<?php echo $this->session->get('csrf');?>" />
		</form>						
	</div>
</div>