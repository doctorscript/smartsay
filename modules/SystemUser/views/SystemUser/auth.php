<!DOCTYPE html>
<html lang="az">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SmartSay.az</title>

    <!-- Bootstrap -->
    <link href="<?php echo $this->url()->base()?>media/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $this->url()->base()?>media/css/style.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="<?php echo $this->url()->base()?>media/js/html5shiv.min.js"></script>
    <script src="<?php echo $this->url()->base()?>media/js/respond.min.js"></script>
    <![endif]-->
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="<?php echo $this->url()->base()?>media/js/jquery-1.11.3.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="<?php echo $this->url()->base()?>media/js/bootstrap.min.js"></script>
	<script src="<?php echo $this->url()->base()?>media/js/jquery.bpopup.min.js"></script>
	<script src="<?php echo $this->url()->base()?>media/js/script.js"></script>
</head>
<body>
<div class="container" id="wrapper">
    <div class="row">
        <div class="col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-12">

            <!--bootstrap modal start-->
            <div class="modal fade modal_green" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="exampleModalLabel"><?php echo $this->translate('password_recovery');?></h4>
                        </div>
                        <div class="modal-body">
                            <form action="<?php echo $this->url()->toRoute('recover_user_password', [
								'lang' => $this->translate()->getLocale()
							])?>" method="post" id="pwd_recover_form">
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label password_restore_hint"><?php echo $this->translate('password_recovery_email');?></label>
                                    <input type="text" class="form-control" id="recipient-name" placeholder="<?php echo $this->translate('enter_email');?>">
									<div id="password_recover_message"></div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="button button_green button_medium" id="pwd_recover"><?php echo $this->translate('send');?></button>
                        </div>
                    </div>
                </div>
            </div>
            <!--bootstrap modal   end-->


            <!--Login wrapper start-->
            <div id="login_wrapper" class="centered">
                <!--login logo start-->
                <div class="centered" style="width: 200px;">
                    <img src="<?php echo $this->url()->base()?>media/img/logo2.png" alt="SmartSay logo"/>
                </div>
                <!--login logo   end-->
                <div id="login_hr"></div>
                <form action="<?php echo $this->url()->toRoute('user_authentication', [
					'lang' => $this->translate()->getLocale()
				])?>" method="POST" style="position:relative;">
                    <!--inputs start-->
                    <div class="custom_inp custom_login_inp">
                        <input type="text" name="email" value="<?php echo $this->escapePost('email');?>" placeholder="Email" autocomplete="off" class="white_placeholder"/>
                    </div>
                    <div class="custom_inp custom_passw_inp">
                        <input type="password" name="password" value="" placeholder="<?php echo $this->translate('password');?>" autocomplete="off" class="white_placeholder"/>
                    </div>
                    <!--inputs   end-->
                    <p id="save_me">
                        <label><?php echo $this->translate('remember_me');?> <input type="checkbox" name="remember_me" value="1"/></label>
                        <span></span>
						 <?php
							if (!empty($errors)) {
								echo '<div class="_error_message" style="position:absolute;top: 104px;left: 45px;'.($this->translate()->getLocale() == 'ru' ? 'width: 250px;' : '').'">';
									foreach ($errors as $error) {
										echo $error;
									}
								echo '</div>';
								if (isset($_POST['remember_me'])) {
									echo '<script>
											$(document).ready(function(){
												$("#save_me span").trigger("click");
											});
										 </script>';
								}
							}
						?>
                    </p>

                    <div id="login_btn">
                        <input class="grey_login_btn" type="submit" name="login" value="<?php echo $this->translate('login');?>"/>
                    </div>
                </form>
                <p style="text-align: center; margin: 10px 0;"><a href="#" id="forget_pwd" data-toggle="modal" data-target="#exampleModal"><?php echo $this->translate('forget_password');?></a></p>

                <p style="text-align: center; font-size: 15px; font-weight: bold;">
				<a href="<?php echo $this->url()->toRoute('registration', ['lang' => $this->translate()->getLocale()])?>"><?php echo $this->translate('be_registered');?></a></p>
            </div>
            <!--Login wrapper   end-->


            <div class="centered" style="width: 300px; text-align: center; padding-bottom: 10px;">
                <table style="width: 100%;">
                    <tbody>
                    <tr>
                        <td colspan="2"><p
                                style="text-align: center; color: #919395; padding: 4px 0; font-size: 15px;">
                            <?php echo $this->translate('coming_soon');?></p></td>
                    </tr>
                    <tr>
                        <td><a href="#"><img src="<?php echo $this->url()->base()?>media/img/app_store.png" alt="AppStore"></a></td>
                        <td><a href="#"><img src="<?php echo $this->url()->base()?>media/img/play_market.png" alt="PlayMarket"></a></td>
                    </tr>
                    </tbody>
                </table>
				<p style="text-align:center;color:#A9ABAE;"><?php echo $this->translate('for_additional_info');?>: <a style="color:#64A039;" href="mailto:info@smartsay.az">info@smartsay.az</a></p>
            </div>

        </div>
    </div>
</div>
</body>
</html>