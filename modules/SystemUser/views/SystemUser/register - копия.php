<?php
if (empty($errors) && !empty($_POST)) {
	echo 'REGISTERED!';
}
?>
<form action="" method="post">
	<input type="text" name="name" placeholder="name" /> <br />
	<input type="text" name="surname" placeholder="surname" /> <br />
	<input type="text" name="email" placeholder="email"/> <br />
	<input type="text" name="password" placeholder="password"/> <br />
	<input type="text" name="c_password" placeholder="c_password"/> <br />
	<input type="text" name="captcha" placeholder="captcha" /> <img src="<?php echo $this->url()->toRoute('system_user_captcha', [
		'lang' 	  => $this->translate()->getLocale(),
		'handler' => 'registration'
	])?>" /> <br />
	<input type="submit" />
</form>