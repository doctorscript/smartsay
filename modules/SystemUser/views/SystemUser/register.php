<div style="position: relative;">
	<h3><?php echo $this->translate('register_in_system');?></h3>
</div>
<?php
	if (!empty($errors)) {
		foreach ($errors as $error) {
			echo '<div class="_error_message" style="text-align:left;">'.$error.'</div>';
		}
	}
?>
<div class="row">
	<!--reg form start-->
	<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
		<div class="u_reg_wr">
			<div class="pull_left">
				<form action="" method="POST" autocomplete="off">
					<div class="custom_inp">
						<input class="white_placeholder" type="text" name="name" value="<?php echo $this->escapePost('name');?>" placeholder="<?php echo $this->translate('name');?>" />
					</div>

					<div class="custom_inp">
						<input class="white_placeholder" type="text" name="surname" value="<?php echo $this->escapePost('surname');?>" placeholder="<?php echo $this->translate('surname');?>" />
					</div>

					<div class="custom_inp">
						<input class="white_placeholder" type="text" name="email" value="<?php echo $this->escapePost('email');?>" placeholder="E-mail" />
					</div>

					<div class="custom_inp">
						<input class="white_placeholder" type="password" name="password" value="" placeholder="<?php echo $this->translate('password');?>" />
					</div>

					<div class="custom_inp">
						<input class="white_placeholder" type="password" name="c_password" value="" placeholder="<?php echo $this->translate('password_again');?>" />
					</div>

					<div class="custom_inp clearfix">
						<p class="pull_left">
							<img src="<?php echo $this->url()->toRoute('system_user_captcha', [
													'lang' 	  => $this->translate()->getLocale(),
													'handler' => 'registration'
												])?>" alt="Captcha"/>
						</p>
						<p class="pull_left"><input class="white_placeholder" type="text" name="captcha" value="" placeholder="<?php echo $this->translate('code');?>" /></p>
					</div>
					<br />
					<br />
					<div>
						<input type="submit" name="submit" value="<?php echo $this->translate('be_registered');?>" class="button button_grey button_block" />
					</div>
				</form>

			</div>
		</div>
	</div>
	<!--reg form   end-->
	
	<!--description start-->
	<div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
		<div class="u_reg_wr">
			<!--right side start-->
			<div class="pull_right">
				<ul>
					<li>
						<p><img src="<?php echo $this->url()->base()?>media/img/basket31.png" alt="Bags" /><?php echo $this->translate('what_is_smartsay');?></p>
					</li>
					<li>
						<p><img src="<?php echo $this->url()->base()?>media/img/payment3.png" alt="Bags" /><?php echo $this->translate('bonus_info');?></p>
					</li>
					<li>
						<p><img src="<?php echo $this->url()->base()?>media/img/wallet34.png" alt="Bags" /><?php echo $this->translate('spending_info');?></p>
					</li>
					<li>
						<p><img src="<?php echo $this->url()->base()?>media/img/maths13.png" alt="Bags" /><?php echo $this->translate('spending_follow_info')?></p>
					</li>
					<li>
						<p><img src="<?php echo $this->url()->base()?>media/img/bags.png" alt="Bags" /><?php echo $this->translate('market_discounts_info');?></p>
					</li>
				</ul>
			</div>
			<!--right side   end-->
		</div>
	</div>
	<!--description   end-->
</div>