<?php
class RemoteFileSaver extends FileSaverAbstract
{
	protected $url;
	protected $port;
	
	//единственный метод который можно описать.
	//нужно будет этот класс сделать абстрактным чтоб не реализовывать метод сейв.
	//+ добавить интерфейс. мы можем захотеть подменить реализацию FileSaverAbstract. а хинтовать уже интерфейс.
	public function setDestination($address, $port = 80)
	{
		if(!filter_var($address, FILTER_VALIDATE_URI)){
			$address = gethostbyaddr($address);
			if(!$address){
				exception
			}
		}
		
		$this->address = $address;
		$this->port    = $port;
		//check if uri is valid and available.
		//available uri checks in other method
		//if uri is not valid throws exception
		
		$this->destination = $destination;
	}

	public function save($filePath)
	{
		//а расширение отдашь в колбеке если оно нужно. не нужно - не отдавай.бери просто файл нейм который вернет название без расширения
		$file = new CurlFile();//надо подумать как его можно заполнить.можно подумать о введении понятия options(массив) через конструктор либо сеттер.
		$file->setFile($filePath);
		$file->setName($this->getFileName());
		
		$ch = curl_init($this->destination);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $file);
		return curl_exec($ch);
		
		//implements curl request
		//ты не можешь знать по каким критериям возвращается ответ от удаленного сервера. поэтому не реализуй этот функционал.
		//вообще удаленное подключение может быть к конкретному порту.
	}

	/**
	 * Setting remote transfering file options
	 *
	 * @param array $options
	 * @return void
	*/
	public function setOptions(array $options)
	{
		$this->options = $options;
	}
	
	public function isDestinationAvailable()
	{
		//сервер может быть сторонним и возвращать не 200 код. поэтому реализовывать тоже особого смысла нет.
	}
}

//basic usage
$file = new File();
$transfer = new LocalFileTransfer();//если поменять на remote то аплод должен идти успешно
$transfer->setDestination('some/path');
$transfer->setFilename(function() use ($file){
	return md5(microtime(true)).$file->getExtensionByMimeType();
});
$transfer->save($file->getTmpName());//throws exception if file is not exists