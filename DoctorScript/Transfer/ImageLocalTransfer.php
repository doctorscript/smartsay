<?php
namespace Core\File\Transfer;

class ImageLocalTransfer extends AbstractLocalFileTransfer
{
	/**
	 * Save image local using PHP GD library
	 *
	 * @param string $filePath
	 * @return true if file successfully saved or false if not
	*/
	
	public function save($filePath)
	{
		if(!is_file($filePath)){
			throw new Exception\FileNotExistsException(
				sprintf('File %s is not exists', $filePath)
			);
		}

		$filename = $this->getFilename();
		
		if(is_file($filePath.$filename)){
			return false;
		}
		
		$ext = pathinfo($filename, PATHINFO_EXTENSION);

		if(!$ext){
			throw new Exception\ExtensionNotProvidedException(
				sprintf('Extension for filename %s not provided', $filename)
			);
		}

		switch($ext){
			case 'jpg':
			case 'jpeg':
				$resource = imagecreatefromjpeg($filePath);

				return imagejpeg($resource, $this->destination.$filename);
			case 'png':
				$resource = imagecreatefrompng($filePath);
				imagealphablending($resource, false);
				imagesavealpha($resource, true);
				
				return imagepng($resource, $this->destination.$filename, 9, PNG_NO_FILTER);
		}

		return false;
	}
}