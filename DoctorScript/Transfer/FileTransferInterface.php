<?php
namespace DoctorScript\Transfer;

interface FileTransferInterface
{
	/** 
	 * Setting destination for file transfer
	 *
	 * @param string
	 * @return void
	 * @throws Exception\InvalidDestinationException if invalid destination given
	 */
	public function setDestination($destination);

	/**
	 * Implements some logic for file saving
	 * For example we can want save file in local folder or on other server 
	 *
	 * @return bool true if file is transfered or false if not
	 */
	public function save($filePath);
}