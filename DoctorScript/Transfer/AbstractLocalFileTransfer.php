<?php
namespace Core\File\Transfer;

abstract class AbstractLocalFileTransfer extends AbstractFileTransfer
{
	/**
	 * Setting directory for save given file 
	 *
	 * @return void
	*/
	public function setDestination($destination)
	{
		if(!is_dir($destination)){
			throw new Exception\InvalidDestinationException(
				sprintf('Directory %s is not exists', $destination)
			);
		}

		$this->destination = $destination;
	}
}