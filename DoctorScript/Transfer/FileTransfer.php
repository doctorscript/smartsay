<?php
namespace DoctorScript\Transfer;

class FileTransfer implements FileTransferInterface
{
	/** 
	 * Name of given file
	 *
	 * @var null|string 
	 */
	protected $filename = null;
	
	/** 
	 * Destination for given file
	 *
	 * @var null|string 
	 */
	protected $destination = null;
	 
	 /** 
	 * Setting name for file
	 *
	 * @param string|integer|callable
	 * @return void
	 * @throws Exception\InvalidFilenameException if given param not string or integer
	 * @throws Exception\InvalidCallbackResultException if $filename is callable and callback result not is string or integer
	 */
	public function setFilename($filename)
	{
		if(!is_string($filename) && !is_int($filename) && !is_callable($filename)){
			throw new Exception\InvalidFilenameException(
				sprintf('Parameter for filename must be string, integer or callable, %s given', gettype($filename))
			);
		}

		if(is_callable($filename)){

			$callbackResult = $filename();

			if(!is_string($callbackResult) && !is_int($callbackResult)){
				throw new Exception\InvalidCallbackResultException(
					sprintf('filename must be string or integer, %s given', gettype($callbackResult))
				);
			}
		}

		$this->filename = isset($callbackResult) ? $callbackResult : $filename;
	}

	/**
	 * Returns name for file
	 * If filename is not set, returns md5 hash based on current unix timestamp
	 *
	 * @return string filename
	 */
	public function getFilename()
	{
		if($this->filename === null){
			$this->filename = md5(time().microtime(true));
		}

		return $this->filename;
	}
	
	/**
	 * Setting directory for save given file 
	 *
	 * @return void
	*/
	public function setDestination($destination)
	{
		if(!is_dir($destination)){
			throw new Exception\InvalidDestinationException(
				sprintf('Directory %s is not exists', $destination)
			);
		}

		$this->destination = $destination;
	}
	
	public function save($filePath)
	{
		return move_uploaded_file($filePath, $this->destination . $this->getFilename());
	}
}