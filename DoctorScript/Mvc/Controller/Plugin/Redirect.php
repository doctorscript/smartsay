<?php
namespace DoctorScript\Mvc\Controller\Plugin;

use DoctorScript\Http\Response\ResponseInterface;
use DoctorScript\ServiceManager\ServiceLocatorInterface;
use DoctorScript\Mvc\Controller\Plugin\MarkerInterface;

class Redirect implements MarkerInterface
{
	private $response;
	private $serviceLocator;
	
	public function __construct(ResponseInterface $response, ServiceLocatorInterface $serviceLocator)
	{
		$this->response 	  = $response;
		$this->serviceLocator = $serviceLocator;
	}
	
	public function toUrl($url)
	{
		$response = $this->response;
		$response->addHeader('Location', $url);
		
		return $response;
	}

	public function toRoute($name, array $params = [])
	{
		$router  = $this->serviceLocator->get('DoctorScript\Router\RouterInterface');
		$baseUrl = $this->serviceLocator->get('config')['baseUrl'];
		$url	 = $router($name, $params);

		return $this->toUrl($baseUrl.$url);
	}
}