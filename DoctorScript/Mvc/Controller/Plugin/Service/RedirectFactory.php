<?php
namespace DoctorScript\Mvc\Controller\Plugin\Service;

use DoctorScript\ServiceManager\ServiceFactoryInterface;
use DoctorScript\ServiceManager\ServiceLocatorInterface;
use DoctorScript\Mvc\Controller\Plugin\Redirect;

class RedirectFactory implements ServiceFactoryInterface
{
	public function createService(ServiceLocatorInterface $serviceLocator)
	{
		$parentLocator = $serviceLocator->getServiceLocator();
		return new Redirect($parentLocator->get('DoctorScript\Http\Response\ResponseInterface'), $parentLocator);
	}
}