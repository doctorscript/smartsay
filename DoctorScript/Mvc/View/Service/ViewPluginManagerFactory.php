<?php
namespace DoctorScript\Mvc\View\Service;

use DoctorScript\ServiceManager\ServiceFactoryInterface;
use DoctorScript\ServiceManager\ServiceLocatorInterface;
use DoctorScript\ServiceManager\Plugin\PluginManagerInterface;
use DoctorScript\Mvc\View\PluginManager;

class ViewPluginManagerFactory implements ServiceFactoryInterface
{
	/**
	 * Create configured view plugin manager
	 *
	 * @param  ServiceLocatorInterface $serviceLocator
	 * @return PluginManagerInterface
	*/
	public function createService(ServiceLocatorInterface $serviceLocator)
	{
		$config = $serviceLocator->get('config');
		$viewPluginConfig = isset($config['view_manager']['plugins']) ? $config['view_manager']['plugins'] : [];
		$manager = new PluginManager($viewPluginConfig);
		$manager->setServiceLocator($serviceLocator);
		
		return $manager;
	}
}