<?php
namespace DoctorScript\Mvc\View\Plugin;

use DoctorScript\ServiceManager\ServiceLocatorInterface;
use DoctorScript\Mvc\View\Plugin\MarkerInterface;

class Url implements MarkerInterface
{
	/**
	 * @var ServiceLocatorInterface
	*/
	private $serviceLocator;
	
	/**
	 * Constructor
	 *
	 * @param ServiceLocatorInterface $serviceLocator
	*/
	public function __construct(ServiceLocatorInterface $serviceLocator)
	{
		$this->serviceLocator = $serviceLocator;
	}
	
	/**
	 * Get base url
	 *
	 * @return string
	 * @throws \RuntimeException if base url is not set
	*/
	public function base()
	{
		$config = $this->serviceLocator->get('config');
		
		if (!isset($config['baseUrl'])) {
			throw new \RuntimeException('Base url is not set');
		}

		return $config['baseUrl'];
	}
	
	/**
	 * Create url by route name and params
	 *
	 * @param string $name
	 * @param array  $params
	 * @return string
	*/
	public function toRoute($name, array $params = [])
	{
		$router  = $this->serviceLocator->get('DoctorScript\Router\RouterInterface');
		$baseUrl = $this->base();
		
		return $baseUrl . $router($name, $params);
	}
}