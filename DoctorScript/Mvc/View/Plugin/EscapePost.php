<?php
namespace DoctorScript\Mvc\View\Plugin;

use DoctorScript\Mvc\View\Plugin\MarkerInterface;

class EscapePost implements MarkerInterface
{
	public function __invoke($key, $default = null)
	{
		if (isset($_POST[$key]) && is_string($_POST[$key])) {
			return htmlspecialchars($_POST[$key]);
		}

		return htmlspecialchars($default);
	}
}