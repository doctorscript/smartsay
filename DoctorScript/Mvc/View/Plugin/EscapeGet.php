<?php
namespace DoctorScript\Mvc\View\Plugin;

use DoctorScript\Mvc\View\Plugin\MarkerInterface;

class EscapeGet implements MarkerInterface
{
	public function __invoke($key, $default = null)
	{
		if (isset($_GET[$key]) && is_string($_GET[$key])) {
			return htmlspecialchars($_GET[$key]);
		}

		return htmlspecialchars($default);
	}
}