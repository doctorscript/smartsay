<?php
namespace DoctorScript\Mvc\View\Plugin\Service;

use DoctorScript\ServiceManager\ServiceFactoryInterface;
use DoctorScript\ServiceManager\ServiceLocatorInterface;
use DoctorScript\Mvc\View\Plugin\Url;

class UrlFactory implements ServiceFactoryInterface
{
	public function createService(ServiceLocatorInterface $serviceLocator)
	{
		return new Url($serviceLocator->getServiceLocator());
	}
}