<?php
namespace DoctorScript\Format;

class Json implements FormatInterface
{
	const FORMAT_CONTENT_TYPE = 'application/json';

	private $variables = [];
	private $options   = [];
	
	public function __construct(array $variables = [], array $options = [])
	{
		if (count($variables) > 0) {
			$this->setVariables($variables);
		}
		
		if (count($options) > 0) {
			$this->setOptions($options);
		}
	}

	public function setVariables(array $variables = [])
	{
		$this->variables = $variables;
	}

	public function addVariable($name, $value)
	{
		$this->variables[$name] = $value;
	}

	public function encode($option = 0, $depth = 512)
	{
		return json_encode($this->variables, $option, $depth);
	}

	public function setOptions(array $options)
	{
		$this->options = $options;
	}

	public function getOptions()
	{
		return $this->options;
	}

	public function renderFormat()
	{
		extract($this->getOptions());
		$optionOption = isset($option) ? $option : JSON_UNESCAPED_SLASHES;
		$optionDepth  = isset($depth) ? $depth : 512;

		return $this->encode($optionOption, $optionDepth);
	}
}