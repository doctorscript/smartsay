<?php
namespace DoctorScript\Paginator;

class Paginator
{
	/**
	 * Current page number
	 *
	 * @var integer $currentPage
	*/
	private $currentPage;
	
	/**
	 * All records count
	 *
	 * @var integer $recordsCount
	*/
	private $recordsCount;
	
	/**
	 * Records limit for per page
	 *
	 * @var integer $perPageLimit
	*/
	private $perPageLimit;
	
	/**
	 * Maximum pages count in paginator
	 *
	 * @var integer $maxPagesCount
	*/
	private $maxPagesCount;
	
	/**
	 * Total pages count in pagination range
	 *
	 * @var integer $pagesCount
	*/
	private $pagesCount;
	
	/**
	 * Constructor
	 *
	 * Provide quick pagination configuration
	 * @param array $config
	*/
	public function __construct(array $config = [])
	{
		foreach ($config as $param => $value) {
			$method = 'set'.ucfirst($param);
			if (method_exists($this, $method)) {
				$this->$method($param);
			}
		}
	}

	/**
	 * Set current page
	 *
	 * @param integer $page
	 * @return void
	*/
	public function setCurrentPage($page)
	{
		$this->currentPage = $page;
		return $this;
	}
	
	/**
	 * Set records count
	 *
	 * @param integer $page
	 * @return void
	*/
	public function setRecordsCount($recordsCount)
	{
		$this->recordsCount = $recordsCount;
		return $this;
	}
	
	/**
	 * Set per page records limit
	 *
	 * @param integer $perPageLimit
	 * @return void
	*/
	public function setPerPageLimit($perPageLimit)
	{
		$this->perPageLimit = $perPageLimit;
		return $this;
	}
	
	/**
	 * Set maximum pages count in paginator range
	 *
	 * @param integer $maxPagesCount
	 * @return void
	*/
	public function setMaxPagesCount($maxPagesCount)
	{
		$this->maxPagesCount = $maxPagesCount;
		return $this;
	}

	/**
	 * Calculate pages range
	 *
	 * @return array page range
	*/
	private function getPageRange()
	{
		$this->pagesCount = ceil($this->recordsCount / $this->perPageLimit);

		$firstPageInRange = $this->currentPage - (int)($this->maxPagesCount / 2);
		$firstPageInRange = $this->pagesCount - $firstPageInRange < $this->maxPagesCount ? 
							$this->pagesCount - $this->maxPagesCount + 1 : $firstPageInRange;
		$firstPageInRange = $firstPageInRange < 1 ? 1 : $firstPageInRange;

		$lastPageInRange = $firstPageInRange + ($this->maxPagesCount - 1);
		$lastPageInRange = $lastPageInRange > $this->pagesCount ? $this->pagesCount : $lastPageInRange;
		$lastPageInRange = $lastPageInRange <= 0 ? 1 : $lastPageInRange;
		
		return range($firstPageInRange, $lastPageInRange);
	}
	
	/**
	 * Get paginator pages
	 *
	 * @return array pages
	*/
	public function getPages()
	{
		$pages = [
			'current' => $this->currentPage,
			'pages'   => $this->getPageRange(),
		];

		$prevPage = $this->currentPage != 1 ? $this->currentPage - 1 : null;
		$nextPage = $this->currentPage < $this->pagesCount ? $this->currentPage + 1 : null;
		$lastPage = $nextPage ? $this->pagesCount : null;

		!$prevPage ?: $pages['prev'] = $prevPage;
		!$nextPage ?: $pages['next'] = $nextPage;
		!$lastPage ?: $pages['last'] = $lastPage;

		return $pages;
	}
}