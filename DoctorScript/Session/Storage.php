<?php
namespace DoctorScript\Session;

class Storage extends \ArrayObject implements StorageInterface
{
	/**
	 * Constructor
	 *
	 * Allows to set user defined session handler
	 *
	 * @param SessionHandlerInterface $handler
	*/
	public function __construct(SessionHandlerInterface $handler = null, $name = null)
	{
		if ($handler !== null){
			session_set_save_handler($handler, true);
		}

		if (session_status() != PHP_SESSION_ACTIVE){
			//$this->setUnlimitedSession();
			session_start();
		}
		
		if ($name !== null) {
			session_name($name);
		}
	}
	
	/** 
		dynamic set unlimited session and change save path. in future that goes in config
		1. Session config must me setted with:
		foreach ($sessionConfig as $config => $value) {
			ini_set($config, $value);
		}
		2. Add event listener before session start using integration that Storage with event manager
		call $em->trigger(StorageInterface::EVENT_SESSION_START)
	*/
	private function setUnlimitedSession()
	{
		/* if (isset($_GET['format']) && is_string($_GET['format']) && $_GET['format'] == 'json') {
			ini_set('session.save_path', $_SERVER['DOCUMENT_ROOT'] . '/../tmp');
			ini_set('session.gc_maxlifetime', 0x7FFFFFFF);
			ini_set('session.cookie_lifetime', 0x7FFFFFFF);
			ini_set('session.gc_probability', 100);
			ini_set('session.gc_divisor', 1);
		} */
	}
	
	/**
	 * Set session data by key
	 *
	 * @param string|integer $key
	 * @param mixed $data
	 * @return void
	*/
	public function set($key, $data)
	{
		$_SESSION[$key] = $data;
	}
	
	/**
	 * Get data from session by key
	 *
	 * @param string|integer $key
	 * @return mixed if session has needed key return appropriate data or false if not
	*/
	public function get($key)
	{
		return $this->has($key) ? $_SESSION[$key] : false;
	}

	/**
	 * Check if session key exists
	 *
	 * @param string|integer $key
	 * @return bool true if session key exists or false if not
	*/
	public function has($key)
	{
		return array_key_exists($key, $_SESSION);
	}

	/**
	 * Delete session data by key
	 *
	 * @param string|integer $key
	 * @return bool true if session data is deleted or false if not
	*/
	public function delete($key)
	{
		if($this->has($key)){
			unset($_SESSION[$key]);
			return !array_key_exists($key, $_SESSION);
		}

		return false;
	}
	
	/**
	 * Regenerate current session id
	 *
	 * @param bool $deleteOldSession
	 * @return bool
	*/
	public function regenerate($deleteOldSession = false)
	{
		return session_regenerate_id($deleteOldSession);
	}
}