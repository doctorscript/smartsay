<?php
namespace DoctorScript\Session;

interface StorageInterface
{
	/**
	 * Set session data by key
	 *
	 * @param string|integer $key
	 * @param mixed $data
	 * @return void
	*/
	public function set($key, $data);
	
	/**
	 * Get data from session by key
	 *
	 * @param string|integer $key
	 * @return mixed if session has needed key return appropriate data or false if not
	*/
	public function get($key);
	
	/**
	 * Check if session key exists
	 *
	 * @param string|integer $key
	 * @return bool true if session key exists or false if not
	*/
	public function has($key);
	
	/**
	 * Delete session data by key
	 *
	 * @param string|integer $key
	 * @return void
	*/
	public function delete($key);
	
	/**
	 * Regenerate current session id
	 *
	 * @param bool $deleteOldSession
	 * @return bool
	*/
	public function regenerate($deleteOldSession = false);
}