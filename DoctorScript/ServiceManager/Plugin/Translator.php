<?php
namespace DoctorScript\ServiceManager\Plugin;

use DoctorScript\Mvc\Controller\Plugin\MarkerInterface as ControllerPluginMarkerInterface;
use DoctorScript\Mvc\View\Plugin\MarkerInterface as ViewPluginMarkerInterface;

class Translator implements ControllerPluginMarkerInterface, ViewPluginMarkerInterface
{
	protected $phrases = [];
	protected $paths   = [];
	protected $locale;

	public function __construct(array $paths)
	{
		$this->paths = $paths;
	}

	public function translate($key)
	{
		if (!isset($this->phrases[$this->locale])) {
			$phrases = [];
			
			foreach ($this->paths as $path) {
				$path = realpath($path);
				if ($path === false) {
					throw new \RuntimeException(sprintf(
						'Invalid translator path %s provided', $path
					));
				}
				$filePath = $path . DIRECTORY_SEPARATOR . $this->locale . '.php';
				if (is_file($filePath)) {
					$filePhrases = include_once($filePath);
					$phrases = array_merge($phrases, $filePhrases);
				}
			}
			
			$this->phrases[$this->locale] = $phrases;
		}

		return isset($this->phrases[$this->locale][$key]) ? $this->phrases[$this->locale][$key] : $key;
	}

	public function __invoke($key)
	{
		return $this->translate($key);
	}

	public function setLocale($locale)
	{
		$this->locale = $locale;
	}

	public function getLocale()
	{
		return $this->locale;
	}
}