<?php
namespace DoctorScript\ServiceManager\Plugin\Service;

use DoctorScript\ServiceManager\ServiceFactoryInterface;
use DoctorScript\ServiceManager\ServiceLocatorInterface;
use DoctorScript\ServiceManager\Plugin\Translator;

class TranslatorFactory implements ServiceFactoryInterface
{
	public function createService(ServiceLocatorInterface $serviceLocator)
	{
		$config = $serviceLocator->get('config')['translator'];
		$paths  = isset($config['paths']) ? $config['paths'] : [];
		
		return new Translator($paths);
	}
}