<?php
namespace DoctorScript\ServiceManager\Test;

class InvokableService
{
	public function helloFromInvokable()
	{
		return 'Invokable service says hello!';
	}
}