<?php
namespace DoctorScript\ServiceManager\Test;

use DoctorScript\ServiceManager\ServiceManager;

class ServiceManagerTest extends \PHPUnit_Framework_TestCase
{
	private $sm;

	public function setUp()
	{
		$serviceConfig = include('service_config.php');
		$this->sm      = new ServiceManager($serviceConfig);
	}
	
	public function testGetNotExistsService()
	{
		$this->setExpectedException('DoctorScript\ServiceManager\Exception\ServiceNotFoundException');
		$this->sm->get('notExistsServiceNamefsffsf');
	}
	
	public function testMissingServiceClass()
	{
		$this->setExpectedException('DoctorScript\ServiceManager\Exception\MissingServiceClassException');
		$this->sm->get('missingClassService');
	}
	
	public function testGetInvalidServiceType()
	{
		$this->setExpectedException('DoctorScript\ServiceManager\Exception\InvalidServiceTypeException');
		$this->sm->get('invalidServiceType');
	}
	
	public function testGetCallableService()
	{
		$callableService1   = $this->sm->get('callableService');
		$callableService2   = $this->sm->get('callableService');
		$newCallableService = $this->sm->get('callableService', false);
		
		$this->assertTrue($callableService1 === $callableService2);
		$this->assertFalse($callableService1 === $newCallableService);
	}
	
	public function testGetFactoryService()
	{
		$factoryService1   = $this->sm->get('factoryService');
		$factoryService2   = $this->sm->get('factoryService');
		$newFactoryService = $this->sm->get('factoryService', false);
		
		$this->assertTrue($factoryService1 === $factoryService2);
		$this->assertFalse($factoryService1 === $newFactoryService);
	}
	
	public function testGetInvokableService()
	{
		$invokableService1   = $this->sm->get('invokableService');
		$invokableService2   = $this->sm->get('invokableService');
		$newInvokableService = $this->sm->get('invokableService', false);
		
		$this->assertTrue($invokableService1 === $invokableService2);
		$this->assertFalse($invokableService1 === $newInvokableService);
	}
}