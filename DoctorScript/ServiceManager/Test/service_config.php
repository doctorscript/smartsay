<?php
return [
	'callableService' => function(){
		$std = new \stdclass;
		$std->result = 'It`s Work!';
		return $std;
	},
	'factoryService' => 'DoctorScript\ServiceManager\Test\ServiceFactory',
	'invokableService' => 'DoctorScript\ServiceManager\Test\InvokableService',
	'missingClassService' => 'Missing\Class',
	'invalidServiceType' => function($sm){
		return array();
	}
];