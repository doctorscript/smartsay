<?php
namespace DoctorScript\Captcha;

class Image implements AdapterInterface
{	
	/**
	 * GRB color of text on image
	 * @var array
	*/
	private $textColor;
	
	/**
	 * GRB color of image background
	 * @var array
	*/
	private $bgColor;

	/**
	 * Path to ttf font file
	 * @var string
	*/
	private $fontPath;
	
	/**
	 * Width of image
	 * @var integer
	*/
	private $width; 
	
	/**
	 * Height of image
	 * @var integer
	*/
	private $height;
	
	/**
	 * Text padding left on image
	 * @var integer	
	*/
	private $paddingLeft;
	
	/**
	 * Text padding top on image
	 * @var integer	
	*/
	private $paddingTop;
	
	/**
	 * Captcha code
	 * @var string
	*/
	private $code;
	
	/**
	 * Code angle on image
	 * @var int
	*/
	private $angle = 0;

	/**
	 * Setting background color(RGB) for captcha image
	 *
	 * @param array $bgColor
	 * @return void
	*/
	public function setBgColor(array $bgColor)
	{
		$this->bgColor = $bgColor;
	}
	
	/**
	 * Setting text color(RGB) for captcha image chars
	 *
	 * @param array $textColor
	 * @return void
	*/
	public function setTextColor(array $textColor)
	{
		$this->textColor = $textColor;
	}
	
	/**
	 * Setting path to ttf font file
	 *
	 * @param string $fontPath
	 * @return void
	*/
	public function setFontPath($fontPath)
	{
		$this->fontPath = $fontPath;
	}
	
	/**
	 * Setting font size for captcha image chars
	 *
	 * @param integer $fontSize
	 * @return void
	*/
	public function setFontSize($fontSize)
	{
		$this->fontSize = (int)$fontSize;
	}
	
	/**
	 * Setting width for captcha image
	 *
	 * @param integer $width
	 * @return void
	*/
	public function setWidth($width)
	{
		$this->width = (int)$width;
	}
	
	/**
	 * Setting height for captcha image
	 *
	 * @param integer $height
	 * @return void
	*/
	public function setHeight($height)
	{
		$this->height = (int)$height;
	}
	
	/**
	 * Setting padding left for captcha image chars
	 *
	 * @param integer $paddingLeft
	 * @return void
	*/
	public function setPaddingLeft($paddingLeft)
	{
		$this->paddingLeft = (int)$paddingLeft;
	}
	
	/**
	 * Setting padding top for captcha image chars
	 *
	 * @param integer $paddingTop
	 * @return void
	*/
	public function setPaddingTop($paddingTop)
	{
		$this->paddingTop = (int)$paddingTop;
	}
	
	public function setCode($code)
	{
		$this->code = $code;
	}
	
	public function setCodeCallback(callable $code)
	{
		$this->code = $code();
	}

	public function getCode()
	{
		if (is_null($this->code)) {
			$this->code = substr(md5(microtime(true)), 0, rand(4,5));
		}
		
		return is_callable($this->code) ? call_user_func($this->code) : $this->code;
	}
	
	public function setAngle($angle)
	{
		$this->angle = $angle;
	}
	
	public function getAngle()
	{
		return $this->angle;
	}
	
	/**
	 * Generate captcha image using GD library
	 *
	 * @return void
	*/
	public function render($base64 = false, $transparent = false)
	{
		$image = imagecreatetruecolor($this->width, $this->height);
		
		list($textR, $textG, $textB) = $this->textColor;
		list($bgR, $bgG, $bgB)       = $this->bgColor;
		
		if ($transparent === true) {
			imagealphablending($image, false);
			$bgColor = imagecolorallocatealpha($image, 0, 0, 0, 127);
			imagesavealpha($image, true);
		} else {
			$bgColor = imagecolorallocate($image, $bgR, $bgG, $bgB);
		}
		$textColor = imagecolorallocate($image, $textR, $textG, $textB);

		imagefilledrectangle($image, 0, 0, $this->width, $this->height, $bgColor);
		imagettftext($image, $this->fontSize, $this->getAngle(), $this->paddingLeft, $this->paddingTop, $textColor, $this->fontPath, $this->getCode());

		if ($base64 === true) {
			ob_start();
			imagepng($image);
			$binaryImg = ob_get_clean();
			return base64_encode($binaryImg);
		}
		
		header('Content-Type: image/png');
		imagepng($image);
	}
}