<?php
namespace DoctorScript\Captcha;

interface AdapterInterface
{
	public function render($base64 = false);
}