<?php
namespace DoctorScript\Hydrator\Test;

class User
{
	private $name;
	private $age;
	private $role;
	
	public function getName()
	{
		return $this->name;
	}
	
	public function getAge()
	{
		return $this->age;
	}
	
	public function getRole()
	{
		return $this->role;
	}
}