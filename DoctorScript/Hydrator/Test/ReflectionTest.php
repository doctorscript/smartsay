<?php
namespace DoctorScript\Hydrator\Test;

use DoctorScript\Hydrator\Reflection;

class ReflectionTest extends \PHPUnit_Framework_TestCase
{
	private $hydrator;
	private $entity;
	private $data;
	
	public function setUp()
	{
		$this->hydrator = new Reflection();
		$this->entity   = new User();
		$this->data     = ['name' => 'John Smith', 'age' => 25, 'role' => 'Admin'];
	}

	public function testHydrateArray()
	{
		$this->hydrator->hydrate($this->entity, $this->data);
		
		$name = $this->entity->getName();
		$age  = $this->entity->getAge();
		$role = $this->entity->getRole();
		
		$this->assertTrue($name === $this->data['name']);
		$this->assertTrue($age === $this->data['age']);
		$this->assertTrue($role === $this->data['role']);
	}

	public function testHydrateJson()
	{
		$json = json_encode($this->data);
		$this->hydrator->hydrateJson($this->entity, $json);
		
		$name = $this->entity->getName();
		$age  = $this->entity->getAge();
		$role = $this->entity->getRole();
		
		$this->assertTrue($name === $this->data['name']);
		$this->assertTrue($age === $this->data['age']);
		$this->assertTrue($role === $this->data['role']);
	}

	public function testEntityExport()
	{
		$this->hydrator->hydrate($this->entity, $this->data);
		$data = $this->hydrator->export($this->entity);

		$this->assertTrue($data === $this->data);
	}
}