<?php
namespace DoctorScript\Http\Request;

class Request implements RequestInterface
{
	/**
	 * Http request method
	 * @var string|null
	*/
	private $method;

	const METHOD_GET  = 'GET';
	const METHOD_POST = 'POST';
	
	/**
	 * Get params from PHP $_POST array
	 *
	 * @param null|string|integer $key $_POST key
	 * @param mixed $default
	 * @param bool $isStringExpected true if expected value is string or false if array
	 * @return mixed
	*/
	public function fromPost($key = null, $default = null, $isStringExpected = true)
	{
		return $this->getDataFromArray($_POST, $key, $default, $isStringExpected);
	}
	
	/**
	 * Get params from PHP $_GET array
	 *
	 * @param null|string|integer $key $_POST key
	 * @param mixed $default
	 * @param bool $isStringExpected true if expected value is string or false if array
	 * @return mixed
	*/
	public function fromGet($key = null, $default = null, $isStringExpected = true)
	{
		return $this->getDataFromArray($_GET, $key, $default, $isStringExpected);
	}
	
	/**
	 * Get data from array
	 * If default value is passed, it returns if needed key not exists in array  
	 *
	 * @param mixed $key
	 * @param mixed $default
	 * @param bool  $isStringExpected
	 * @return mixed
	*/
	private function getDataFromArray($arr, $key, $default, $isStringExpected = true)
	{
		if (null === $key){
			return $arr;
		}
		
		if (!isset($arr[$key])){
			return $default;
		}
		
		if ($isStringExpected && !is_string($arr[$key])){
			return $default;
		}

		return $arr[$key];
	}

	/**
	 * Get object that contains uploaded file parameters
	 *
	 * @param string $inputName
	 * @return File\HttpFile
	*/
	public function fromFiles($inputName)
	{
		//check if count $_FILES[$inputName] > 0) return array|arrayobject contains fileset
		
		return new File\HttpFile($inputName);
	}
	
	/**
	 * Get http request method
	 *
	 * @return string http request method
	*/
	public function getMethod()
	{
		return strtoupper($_SERVER['REQUEST_METHOD']);
	}

	/**
	 * Checks if http request method is GET
	 *
	 * @return bool true if request method is GET or false if not
	*/
	public function isGet()
	{
		return $this->getMethod() == self::METHOD_GET;
	}

	/**
	 * Checks if http request method is POST
	 *
	 * @return bool true if request method is POST or false if not
	*/
	public function isPost()
	{
		return $this->getMethod() == self::METHOD_POST;
	}

	public function isPostXhr()
	{
		return ($this->isXhr() && $this->isPost());
	}

	public function isGetXhr()
	{
		return ($this->isXhr() && $this->isGet());
	}

	public function isMethod($method)
	{
		return ($this->getMethod() == strtoupper($method));
	}

	/**
	 * Get client request uri
	 *
	 * @return string request uri
	*/
	public function getUri()
	{
		if (strpos($_SERVER['REQUEST_URI'], '/?') === 0) {
			return '';
		} 
		return ltrim($_SERVER['REQUEST_URI'], '/');
	}
	
	public function isXhr()
	{
		return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
	}
}