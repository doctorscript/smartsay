<?php
namespace DoctorScript\Http\Request\File;

class HttpFile
{
	/**
	 * Uploaded file input name
	 *
	 * @var string
	*/
	protected $inputName;
	
	/**
	 * Uploaded file mime type
	 *
	 * @var string mime type default null
	*/
	protected $mimeType  = null;
	
	/**
	 * Constructor
	 *
	 * @param string $inputName uploaded file input name
	*/
	public function __construct($inputName)
	{
		$this->inputName = $inputName;
	}
	
	/**
	 * Check if file is uploaded using protocol HTTP
	 *
	 * @return bool true if file is uploaded of false if not
	*/
	public function isUploaded()
	{
		if(!array_key_exists($this->inputName, $_FILES)){
			return false;
		}

		if(!is_uploaded_file($this->getTmpName())){
			return false;
		}

		return true;
	}
	
	/**
	 * Get uploaded file temporary name
	 *
	 * @return string uploaded file temporary name
	*/
	public function getTmpName()
	{
		return $_FILES[$this->inputName]['tmp_name'];
	}
	
	/**
	 * Get uploaded file name
	 *
	 * @return string uploaded file name
	*/
	public function getName()
	{
		return $_FILES[$this->inputName]['name'];
	}
	
	/**
	 * Check if uploaded file has errors
	 *
	 * @return bool false if uploaded file has errors or true if not
	*/
	public function isCleanUpload()
	{
		return $_FILES[$this->inputName]['error'] == UPLOAD_ERR_OK;
	}
	
	/**
	 * Get error code of uploaded file
	 *
	 * @return integer error code
	*/
	public function getErrorCode()
	{
		return $_FILES[$this->inputName]['error'];
	}
	
	/**
	 * Get uploaded file mime type using PHP finfo extension
	 *
	 * @return string mime type
	*/
	public function getMimeType()
	{
		if($this->mimeType === null){
			$this->mimeType = (new \finfo(FILEINFO_MIME_TYPE))->file($this->getTmpName());
		}
		
		return $this->mimeType;
	}
	
	/**
	 * Get uploaded file mime type received from client
	 *
	 * @return string mime type
	*/
	public function getClientMimeType()
	{
		return $_FILES[$this->inputName]['type'];
	}
	
	/**
	 * Get uploaded file extension from file name
	 *
	 * @return string fo;e extension
	*/
	public function getExtension()
	{
		return pathinfo(basename($this->getName()), PATHINFO_EXTENSION);
	}
	
	/**
	 * Get uploaded file size in bytes
	 *
	 * @return integer
	*/
	public function getSize()
	{
		return filesize($this->getTmpName());
	}

	/**
	 * Export file object to array
	 *
	 * @return mixed
	*/
	public function toArray()
	{
		if (!$this->isUploaded()) {
			return false;
		}

		return $_FILES[$this->inputName];
	}
}