<?php
namespace DoctorScript\Http;

class HttpFileValidator
{
	const MIME_TYPE_ERROR = true;
	const MIMES_NOT_EQUAL = true;
	const FILE_EXT_ERROR  = true;
	const MIN_SIZE_ERROR  = true;
	const MAX_SIZE_ERROR  = true;
	
	/**
	 * Allowed file mime types list
	 * @var array
	*/
	private $allowedMimes      = [];
	
	/**
	 * Allowed file extensions list
	 * @var array
	*/
	private $allowedExtensions = [];
	
	/**
	 * Allowed minimum file size
	 * @var integer
	*/
	
	private $minSize = 0;
	
	/**
	 * Allowed maximum file size
	 * @var integer
	*/
	private $maxSize = 0;
	
	/**
	 * Validation errors messages
	 * @var array	
	*/
	private $errors  = [];
	
	/**
	 * Set allowed mime types for uploaded file
	 *
	 * @param array $allowedMimes
	 * @return void
	*/
	public function setAllowedMimes(array $allowedMimes)
	{
		$this->allowedMimes = array_merge($this->allowedMimes, $allowedMimes);
	}
	
	/**
	 * Set allowed extensions for uploaded file
	 *
	 * @param array $allowedExtesions
	 * @return void
	*/
	public function setAllowedExtensions(array $allowedExtesions)
	{
		$this->allowedExtensions = array_merge($this->allowedExtensions, $allowedExtensions);
	}
	
	/**
	 * Set minimum size for uploaded file
	 *
	 * @param integer $minSize
	 * @return void
	*/
	public function setMinSize($minSize)
	{
		$this->minSize = (int)$minSize;
	}
	
	/**
	 * Set maximum size for uploaded file
	 *
	 * @param integer $maxSize
	 * @return void
	*/
	public function setMaxSize($maxSize)
	{
		$this->maxSize = (int)$maxSize;
	}
	
	/**
	 * Checks if client mime type is equal to file mime type
	 *
	 * @param HttpFile $file
	 * @return bool true if mimes equal or false if not
	*/
	public function isMimesEqual(HttpFile $file)
	{
		$clientMime = $file->getСlientMimeType();
		$fileMime   = $file->getMimeType();

		return ($headerMime == $fileMime);
	}
	
	/**
	 * Checks if uploaded file is valid
	 *
	 * @param HttpFile $file
	 * @return bool true if file is valid or false if not
	*/
	public function isValid(HttpFile $file)
	{
		if(!in_array($file->getMimeType(), $this->allowedMimes)){
			$this->errors['mime_type'] = self::MIME_TYPE_ERROR;
		}
		
		if(!$this->isMimesEqual($file)){
			$this->errors['mimes_not_equal'] = self::MIMES_NOT_EQUAL;
		}
		
		if(!in_array($file->getExtension(), $this->allowedExtensions)){
			$this->errors['file_ext'] = self::FILE_EXT_ERROR;
		}

		$fileSize = ($file->getSize() / 1024);
		
		if($fileSize < $this->minSize){
			$this->errors['min_size'] = self::MIN_SIZE_ERROR;
		}

		if($fileSize > $this->maxSize){
			$this->errors['max_size'] = self::MAX_SIZE_ERROR;
		}

		return (count($this->errors) === 0);
	}
	
	/**
	 * Get validation errors messages
	 *
	 * @return array
	*/
	public function getErrors()
	{
		return $this->errors;
	}
}