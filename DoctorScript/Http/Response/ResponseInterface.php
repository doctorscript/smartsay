<?php
namespace DoctorScript\Http\Response;

interface ResponseInterface
{
	/**
	 * Add single header to headers list
	 *
	 * @param string $header
	 * @param string $value
	 * @return void
	*/
	public function addHeader($header, $value);
	
	/**
	 * Set headers list
	 *
	 * @param array $headers
	 * @return void
	*/
	public function setHeaders(array $headers);
	
	/**
	 * Set response content using after HTTP headers
	 *
	 * @param mixed $contnet
	 * @return void 
	*/
	public function setContent($content);
	
	/**
	 * Process response HTTP headers and content(if defined) to client.
	 *
	 * @return void
	*/
	public function send();
}