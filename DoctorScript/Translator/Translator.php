<?php
namespace DoctorScript\Translator;

use DoctorScript\Mvc\Controller\Plugin\MarkerInterface as ControllerPluginMarkerInterface;
use DoctorScript\Mvc\View\Plugin\MarkerInterface as ViewPluginMarkerInterface;

class Translator implements TranslatorInterface, ControllerPluginMarkerInterface, ViewPluginMarkerInterface
{
	protected $locale;
	protected $phrases = [];
	protected $paths   = [];
	
	public function __construct(array $paths)
	{
		$this->paths = $paths;
	}

	public function translate($key)
	{
		if (empty($this->phrases)) {
			foreach ($this->paths as $path) {
				$realPath = realpath($path);
				if ($realPath === false) {
					throw new \RuntimeException(sprintf(
						'Invalid translator path %s provided', $path
					));
				}
				$filePath = $realPath . DIRECTORY_SEPARATOR . $this->getLocale() . '.php';
				if (is_file($filePath)) {
					$phrases = require_once($filePath);
					$this->phrases = array_merge($this->phrases, $phrases);
				}
			}
		}
		
		return isset($this->phrases[$key]) ? $this->phrases[$key] : $key;
	}

	public function __invoke($key)
	{
		return $this->translate($key);
	}
	
	public function setLocale($locale)
	{
		$this->locale = $locale;
	}
	
	public function getLocale()
	{
		return $this->locale;
	}
}