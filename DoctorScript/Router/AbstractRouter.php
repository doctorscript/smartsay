<?php
namespace DoctorScript\Router;

abstract class AbstractRouter implements RouterInterface
{
	public function __invoke($name, array $params)
	{		
		if (!isset($this->routes[$name])) {
			throw new \Exception(sprintf('Route %s is not defined', $name));
		}

		$uriAsPattern = $this->routes[$name]['pattern'];
		$patterns	  = [];
		foreach (array_keys($params) as $key) {
			$patterns[] = '#\(\?<'.$key.'>.*?\)#';
		}

		$values = array_values($params);
		return rtrim(preg_replace($patterns, $values, $uriAsPattern), '/?');
	}
}