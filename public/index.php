<?php
use DoctorScript\Mvc\Application;
ini_set('session.use_only_cookies', true);
ini_set('session.cookie_httponly', true);
ini_set('session.cookie_secure', true);
ini_set('session.gc_probability', 100);
ini_set('session.gc_divisor', 1);
session_name('sessid');
ini_set('display_errors', true);
error_reporting(E_ALL | E_STRICT);
chdir(dirname(__DIR__));

require('./DoctorScript/Autoloader/SplAutoloaderInterface.php');
require('./DoctorScript/Autoloader/StandardAutoloader.php');

$loader = new \DoctorScript\Autoloader\StandardAutoloader(['.']);
$loader->register(true, false);
require './DoctorScript/Mvc/Application.php';
Application::init(require('config/application_config.php'))->run();
require 'clog.php';
