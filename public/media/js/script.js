$(document).ready(function(){
var checkbox = $("#save_me input[type=checkbox]");
// custom check/uncheck
$("#save_me span").on('click', function(){
	
	var target = $(this);

	if(target.hasClass('custom_checked')) {
		target.removeClass('custom_checked');
		checkbox.prop('checked', false);
	} else {
		target.addClass('custom_checked');
		checkbox.prop('checked', true);
	}
});

$(window).resize(function () {
	setTimeout(setMarginToMenuSelect, 500)
});

setMarginToMenuSelect();

function setMarginToMenuSelect(){

	   if ($(window).width() < 768) {
		// small screens
			$(".navbar-select").addClass('menu_select_margin');
		} else {
			$(".navbar-select").removeClass('menu_select_margin');
		}

}

/* For custom checkbox */
var checkboxex = $(".custom_ch");
checkboxex.hide();
$.each(checkboxex, function(){
	$(this).wrap("<span class='c_d_w' data-checked='false'></span>");
});

$('.c_d_w').on('click', function(e){
	$('.t_on_gr').removeAttr('checked');
	if($(this).attr('data-checked') === 'true'){
		$(this)
			.attr('data-checked', 'false')
			.removeClass('c_d_w_c')
			.find('input[type="radio"]').removeAttr('checked');
		
	} else{
		$(this)
			.attr('data-checked', 'true')
			.addClass('c_d_w_c')
			.find('input[type="radio"]').attr('checked', 'checked');	
	}
	
	$('.c_d_w').not($(this)).attr('data-checked', 'false');
	
	$('.c_d_w').each(function(){
		if($(this).attr('data-checked') == 'false'){
			$(this)
				.removeClass('c_d_w_c')
				.find('input[type=radio]').prop('checked',false)
			;
		}
	})
	e.preventDefault();
});

$('#pwd_recover').on('click', function(e){
	e.preventDefault();
	var action = $('#pwd_recover_form').attr('action');
	var data   = {email: $('#recipient-name').val()};
	$.post(action, data, function(response){
		if ($.isEmptyObject(response.errors)) {
			var responseMessage = response.successMessage;
			$('#recipient-name').val('');
			$('#password_recover_message').css('color', '#7BBE4D');
		} else {
			var responseMessage = response.errors.email;
			$('#password_recover_message').css('color', '#c00');
		}
		$('#password_recover_message').text(responseMessage).show();
	}, 'json');
});
$('#client_search').on('click', function(e){
	e.preventDefault();
	var selectedMarket = $('.selected_market').attr('data-name');
	var action = $(this).parents('form').attr('action').replace('{market}', selectedMarket);
	var data = {
		card_number: $('#card_number').val(), 
		phone_number: $('#phone_number').val(),
		csrf: $('input[name="csrf"]').val()
	};
	
	$('_success_message_wrapper, ._error_message').hide();
	$('#market_auth_please_wait').show();
	setTimeout(function(){
		$.post(action, data, function(response){
			if ($.isEmptyObject(response.errors)) {
				var name   = '<b>'+response.client.name + ' ' + response.client.surname+'</b>';
				var client = $('#client_name_pattern').text().replace('{name}', name);
				$('._success_message').html(client);
				$('._success_message_wrapper').show();
				var clientAuthLink = $('#client_auth_link').text()
														   .replace('{market}', $('.selected_market')
														   .attr('data-name'));
				$('#see_purchases').attr('href', clientAuthLink);
			} else {
				$('._error_message').html(response.errors.message).show();
				$('._success_message_wrapper').hide();
			}
			
			$('#market_auth_please_wait').hide();
		}, 'json');
	},1000);
});
$('#see_purchases').on('click', function(e){
	e.preventDefault();
	var url  = $(this).attr('href');
	var data = {
		card_number: $('#card_number').val(), 
		phone_number: $('#phone_number').val(),
		csrf: $('input[name="csrf"]').val()
	};
	$.post(url, data, function(response){
		console.log(response);
		//return false;
		if ($.isEmptyObject(response.errors)) {
			var purchasesLink = $('#purchases_link_pattern').text()
															.replace('{market}', $('.selected_market')
															.attr('data-name'));
			location.href = purchasesLink +'?client_id='+ response.client_id;
		} else {
			$('._error_message').html(response.errors.message).show();
			$('._success_message_wrapper').hide();
		}
	});
});

});