$(document).ready(function(){
	var checkbox = $("#save_me input[type=checkbox]");
	// custom check/uncheck
	$("#save_me span").on('click', function(){
		
		var target = $(this);

		if(target.hasClass('custom_checked')) {
			target.removeClass('custom_checked');
			checkbox.prop('checked', false);
		} else {
			target.addClass('custom_checked');
			checkbox.prop('checked', true);
		}
	});
});